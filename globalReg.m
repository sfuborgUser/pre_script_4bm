function volume_mcorr = globalReg(volume, usfac, ref, mcorrpath, fpsave, volNo, numBM)

numFrames = size(volume, 3);
% yShift = zeros(1, size(volume, 3));
volume_mcorr = zeros(size(volume));

% We only want to register the "leading" BM scans to each other, then apply
% the offsets from those to their following BM scans
for jj = 1:numBM:numFrames
%     [output, ~] = dftregistration(fft2(ref), fft2(imgaussfilt(abs(volume(:, :, jj)), 2)), usfac);
	[output, ~] = dftregistration(fft2(ref), fft2(abs(volume(:, :, jj))), usfac);

    yShift(jj) = output(3);
	
	% For shifting the B scans, use nearest neighbour interpolation. The
	% sub pixel interpolation seems to cause horrendous line artifacts,
	% likely caused by bilinear interpolation on complex numbers
	for ii = jj:(jj+numBM-1)
 		volume_mcorr(:, :, ii) = imtranslate(volume(:, :, ii), [0, yShift(jj)], 'nearest');
% 		volume_mcorr(:, :, ii) = circshift(volume(:, :, ii), [yShift(jj), 0]);
	end
	
	% Check to see if the y offset is wacko, if it is, ignore the offset
% 	if (abs(yShift(jj)) > 10)
% 		volume_mcorr(:, :, jj) = volume(:, :, jj);
% 	else
% 		volume_mcorr(:, :, jj) = imtranslate(volume(:, :, jj), [0, yShift(jj)]);
% 	end

end

% save(fullfile(mcorrpath,[fpsave,'_',num2str(volNo-1),'    _mcorr.mat']), 'volume_mcorr', '-v7.3');

end

