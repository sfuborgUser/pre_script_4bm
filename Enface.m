%% Preprocess AO-OCT data
function Enface(filename, mcorrpath, qcpath, gcpath, BVpath, octapath)

addpath(pwd);
%% Load LUT
fn_ResParam = 'LUTSS.dat'; % FOR 100khz DATA USE 'LUTSS.bin'
fid_ResParam = fopen(fullfile(fn_ResParam));
rescaleParam = fread(fid_ResParam, 'double')+1.00;
fclose(fid_ResParam);
% 
% %Read .xml file
% if contains(filename,'unp')
%     filenamexml = filename(1:end-4);
% else
%     filenamexml = filename;
% end

% %cd to RAW folder
% if contains(upper(filename(1:2)), '_')
%     cd(fullfile('/ensc/IMAGEBORG/DATA/ECC/RAW', upper(filename(9:12))));
% else
%     cd(fullfile('/ensc/IMAGEBORG/DATA/ECC/RAW', upper(filename(10:13))));
% end
% 
% Folder = pwd;
% FileList = dir(fullfile(Folder, '**', '*.xml'));
%     
% %To find RAW directory
% for ii = 1:size(FileList, 1)
%     if strcmp(strcat(filename, '.xml'), FileList(ii).name)
%         directory = FileList(ii).folder;
%     end
% end
% 
% cd(directory);

%To find unp filenames
% unp_list = dir(fullfile(directory, '*.unp'));

% fn2 = strcat(filenamexml,'.xml');
% parameters  = getParameters(fn2);

% options.rescaleParam        = rescaleParam;
% options.scanType            = 0; % single side scan = 0 / double side scan = 1
% options.calSigFilter        = [11 15]; %11/15
% options.dispMaxOrder        = 5;
% options.coeffRange          = 20;
% options.dim                 = [parameters(1) parameters(2)];
% options.ffttype             = 2;
% options.totalFrames         = parameters(3);
% options.stFrame             = 1;
% options.Nsteps              = 10;
% options.vol                 = parameters(4);



if contains(filename,'unp')
    fpsave = filename(1:end-4);
else
    fpsave = filename;
end

fpsave = filename(1:end-4);

Nsteps = 10;

for volNo = 1:10
    
    % find the unp files with underscores
    %% NEED TO TALK TO YIFAN/MORGAN ABOUT OCT Viewer NAMING CONVENTION
   
%     AD_gc_script_1BM(BVpath,fpsave,gcpath,Nsteps,volNo-1)


%     try
%         load(fullfile(mcorrpath,[fpsave,'_',num2str(volNo-1),'    _mcorr.mat']));
        load(fullfile(gcpath,[fpsave,'_',num2str(volNo-1),'-ILMseg.mat']));
        load(fullfile(gcpath,[fpsave,'_',num2str(volNo-1),'-ONLseg.mat']));
        load(fullfile(octapath,[fpsave,'_',num2str(volNo-1),'_cplxOCTA.mat']));

%         V2 = abs(volume_mcorr);
%         
%         v3(:,:,:,1) = V2(:,:,1:4:end);
%         v3(:,:,:,2) = V2(:,:,2:4:end);
%         v3(:,:,:,3) = V2(:,:,3:4:end);
%         v3(:,:,:,4) = V2(:,:,4:4:end);
%         volume = squeeze(mean(v3,4));
% 
%         volume_mcorr = volume;

%         for i = 1:size(volume_mcorr,2)
%             for j = 1:size(volume_mcorr,3)
%                 fund_all(i,j) = mean(volume_mcorr(gcONL(i,j):gcONL(i,j)+5,i,j),1);
%             end
%         end
        
		%for RPE
%         for i = 1:size(volume_mcorr,2)
%             for j = 1:size(volume_mcorr,3)
%                 fund_RPE(i,j) = mean(volume_mcorr(gcONL(i,j)+7:gcONL(i,j)+13,i,j),1);
%             end
% 		end

		for i = 1:size(cplxOCTA,2)
            for j = 1:size(cplxOCTA,3)
                fund_all(i,j) = mean(cplxOCTA(gcILM(i,j):gcONL(i,j)-20,i,j),1); %INL was +10
            end
        end
	
        % save data
        if ~exist(qcpath,'dir')
			mkdir(qcpath);
		end
		

		imwrite((imadjust(mat2gray(fund_all))),fullfile(qcpath,[filename(1:end-4),'_',num2str(volNo-1),'_all.tif']));
		
%         imwrite(imadjust(mat2gray(abs(fund_all))),fullfile(qcpath,[filename(1:end-4),'_',num2str(volNo-1),'_PR.tif']));
%         imwrite(imadjust(mat2gray(fund_RPE)),fullfile(qcpath,[filename(1:end-4),'_',num2str(volNo-1),'_RPE.tif']));
%     catch
%         warning('En Face Creation failed!')
%     end


    
end
end




