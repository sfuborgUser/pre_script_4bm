%% Preprocess AO-OCT data
% Enter the .xml file name as a string 
% For example '13_38_06-H866-OD-NAS-SUP-NFL'
function Pre_script(filename)

addpath(pwd);
%% Load LUT
fn_ResParam = 'LUTSS.bin'; 
fid_ResParam = fopen(fullfile(fn_ResParam));
rescaleParam = fread(fid_ResParam, 'double')+1.00;
fclose(fid_ResParam);

% Read .xml file
if contains(filename,'unp')
    filenamexml = filename(1:end-4);
else
    filenamexml = filename;
end

% cd to RAW folder
if contains(upper(filename(1:2)), '_')
    cd(fullfile('/ensc/IMAGEBORG/DATA/ECC/RAW', upper(filename(9:12))));
else
    cd(fullfile('/ensc/IMAGEBORG/DATA/ECC/RAW', upper(filename(10:13))));
end

Folder = pwd;
FileList = dir(fullfile(Folder, '**', '*.xml'));
    
% Find the raw folder that we want
for ii = 1:size(FileList, 1)
    if strcmp(strcat(filename, '.xml'), FileList(ii).name)
        directory = FileList(ii).folder;
    end
end

cd(directory);

%To find unp filenames
unp_list = dir(fullfile(directory, '*.unp'));

fn2 = strcat(filenamexml,'.xml');
parameters  = getParameters(fn2);

options.rescaleParam        = rescaleParam;
options.scanType            = 0; % single side scan = 0 / double side scan = 1
options.calSigFilter        = [11 15]; %11/15
options.dispMaxOrder        = 5;
options.coeffRange          = 20;
options.dim                 = [parameters(1) parameters(2)];
options.ffttype             = 2;
options.totalFrames         = parameters(3);
options.stFrame             = 1;
options.Nsteps              = 10;
options.vol                 = parameters(4);

% Set file paths
rawpath = fullfile(directory);
qcpath = fullfile(directory(1:25), 'fundus', directory(29:end), num2str(parameters(8)));
mcorrpath = fullfile(directory(1:25), 'mcorr', directory(29:end));
octapath = fullfile(directory(1:25), 'sv', directory(29:end));
BVpath = fullfile(directory(1:25), 'BVsmooth', directory(29:end));
gcpath = fullfile(directory(1:25), 'Segment_gc', directory(29:end));

addpath(rawpath)

if contains(filename,'unp')
    fpsave = filename(1:end-4);
else
    fpsave = filename;
end

VOL=options.vol;
Nsteps = options.Nsteps;

for volNo = 1:VOL
    
    % Find the unp files with underscores
    %% NEED TO TALK TO YIFAN/MORGAN ABOUT OCT Viewer NAMING CONVENTION
	%% Nvm lol they don't wanna talk about it so I have to deal with it
    for jj = 1:size(unp_list, 1)
        if strcmp(unp_list(jj).name(1:end-4), strcat(filename, '_', num2str(volNo-1)))
            filename_unp = unp_list(jj).name;
            break;
        else
            filename_unp = filename;
        end
    end
    
    disp(['Processing volume ', num2str(volNo-1),' of ',fpsave ]);
    
    if (volNo > 1)
        options.stFrame = options.stFrame + options.totalFrames;
    else
        options.stFrame = 1;
    end
    
    %% MOTION CORRECTION
     if ~exist(fullfile(mcorrpath,[fpsave,'_',num2str(volNo-1),'    _mcorr.mat']),'file')

        disp('Starting Motion Correction');

        if ~exist(mcorrpath,'dir')
            mkdir(mcorrpath);
        end
        options.mcorr = 1;	
        cd(rawpath)

        %Run the RAW to mcorr scripts
        tic

		% So why do we have this?
		% It's because the naming convention seems to always be different
		% Some .unp files are named exactly the same as their .xml files
		% Others have _0 appended (pfft for whatever reason -,-)
        if strcmp(filename_unp, filenamexml)
            if (parameters(5) == 1)
                ProcdData = proCplxAOOCT_SAO(strcat(fpsave,'.unp'),options);
            else
                ProcdData = proCplxAOOCT(strcat(fpsave,'.unp'),options);
            end
        else
            if (parameters(5) == 1)
                ProcdData = proCplxAOOCT_SAO(strcat(fpsave,'_',num2str(volNo-1),'.unp'),options);
            else
                ProcdData = proCplxAOOCT(strcat(fpsave,'_',num2str(volNo-1),'.unp'),options);
            end
        end

        toc

		% DFT registration
		% Create a reference frame (usually frame 20 for da lols)
		% Do global registration (all frames registered to 1 frame
		% Then do local registration (register BM scans to each other)
		
		% Could it be possible that the way we are doing local/global
		% registration is screwing up the variance?? What if these tiny sub
		% pixel shifts between the b scans are causing abnormal variance
		% between BM scans? In that case we should be registering only the
		% first "leading" BM scans to each other and applying the same 
		% offsets to their following BM scans
		
        usfac = 10;
        ref = imgaussfilt(abs(ProcdData(:, :, 20)), 2);
		try
			volume_mcorr = globalReg(ProcdData, usfac, ref, mcorrpath, fpsave, volNo, parameters(5)); 
			volume_mcorr = localReg(volume_mcorr, usfac, mcorrpath, fpsave, volNo, parameters(5));
		catch
			disp('Error in DFT registration, saving ProcdData instead');
			save(fullfile(mcorrpath,[fpsave,'_',num2str(volNo-1),'    _Procd_mcorr.mat']), 'ProcdData', '-v7.3');
		end
		
		save(fullfile(mcorrpath,[fpsave,'_',num2str(volNo-1),'    _mcorr.mat']), 'volume_mcorr', '-v7.3');

		clear ProcdData;

		
        %% Check y-orientation of volume
		%% This could use improvement
		% Take the average intensity of a relative b scan (saaay frame 20)
		% along the columns
        avg_intensity = mean(abs(volume_mcorr(:, :, 20)), 2);

		% Identify where the peaks are, note the index of the largest peak,
		% this will usually be the PR layer
        [pks] = findpeaks(avg_intensity);
        dex1 = find(pks == max(pks));
		
		% Set the largest peak (and surrounding area) to 0
		% Check to make sure you aren't indexing out of range
		if (dex1 <= 5)
			pks(1:dex1+5) = 0;
			
		elseif (dex1+5 >= size(volume_mcorr, 1))
			pks(dex1:end) = 0;
			
		else
			pks(dex1-5:dex1+5) = 0;
		end
		
		% Find the next largest peak, this will usually be the ILM
        dex2 = find(pks == max(pks));
        [pks] = findpeaks(avg_intensity);

        % find indices of peaks 
        index1 = find(avg_intensity == pks(dex1));
        index2 = find(avg_intensity == pks(dex2));

        % flipping condition
		if (index2 > index1)
            volume_mcorr = flip(volume_mcorr, 1);
            disp('volume_mcorr flipped')
		end
		
        save(fullfile(mcorrpath,[fpsave,'_',num2str(volNo-1),'    _mcorr.mat']), 'volume_mcorr', '-v7.3');

        disp('Motion Correction Finished')
        clear volume_mcorr;
    
    else
        disp('Mcorr file already exists.')
    end

	
    %% OCTA Processing
     if ~exist(fullfile(octapath,[fpsave,'_',num2str(volNo-1),'_cplxOCTA.mat'])) && parameters(5) > 1

        try
            disp('Starting OCT-A Processing')

            cd(rawpath)
            tic

            if ~exist(octapath, 'dir')
                mkdir(octapath);
            end
			  
            cplxOCTA = proCplxOCTA(strcat(fpsave,'_',num2str(volNo-1),'.unp'),octapath,mcorrpath);
            toc

            disp('OCT-A volume finished')
        catch
            warning('OCTA Processing failed!')
        end

    else
        disp('Alreadys exists or not an OCT-A Volume')
    end  
    
    %% BV SMOOTHING
     if ~exist(fullfile(BVpath,[fpsave,'_',num2str(volNo-1),strcat('-BV_it',num2str(Nsteps),'.mat')]))

        disp('Starting BVSmoothing')
        load(fullfile(mcorrpath,[strcat(fpsave,'_',num2str(volNo-1)),'    _mcorr.mat']));
        
        V2 = volume_mcorr;
            
        % Average volume_mcorr
        if parameters(5) == 1
            %no averaging BM 
            volume = volume_mcorr;
        elseif parameters(5) == 2
            v3(:,:,:,1) = V2(:,:,1:2:end);
            v3(:,:,:,2) = V2(:,:,2:2:end);
            V4 = squeeze(mean(v3,4));
            volume = V4;
        elseif parameters(5) == 4
            v3(:,:,:,1) = V2(:,:,1:4:end);
            v3(:,:,:,2) = V2(:,:,2:4:end);
            v3(:,:,:,3) = V2(:,:,3:4:end);
            v3(:,:,:,4) = V2(:,:,4:4:end);
            volume = squeeze(mean(v3,4));
        end
        
        clear V4; clear v3; clear V2;
        volume_mcorr = abs(volume);

        V = BVsmooth3D((volume_mcorr),1,0.1,Nsteps);

        % Save data
        if ~exist(BVpath,'dir')
            mkdir(BVpath);
        end
        save(fullfile(BVpath,[fpsave,'_',num2str(volNo-1),strcat('-BV_it',num2str(Nsteps),'.mat')]), 'V', '-v7.3');
        clear volume_mcorr; clear V;
        clear volume;
        disp('BVsmoothing Finished')
    else
        disp('BVsmooth file already exists.')
    end
    
    %% AUTOMATED SEGMENTATION
    if ~exist(fullfile(gcpath,[fpsave,'_',num2str(volNo-1),'-ONLseg.mat'])) % && ~exist(fullfile(gcpath,[fpsave,'_',num2str(volNo),'-ILMseg.mat']))
        if (parameters(5) == 4)
			disp('Starting Automated Segmentation 4 BM')
			if ~exist(gcpath,'dir')
				mkdir(gcpath);
			end

			AD_gc_script_4BM(BVpath,fpsave,gcpath,Nsteps,volNo-1)
			disp('Automated Segmentation Finished')
		
		elseif(parameters(5) == 2)
				disp('Starting Automated Segmentation 2 BM lol')
			if ~exist(gcpath,'dir')
				mkdir(gcpath);
			end

			AD_gc_script_2BM(BVpath,fpsave,gcpath,Nsteps,volNo-1)
			disp('Automated Segmentation Finished')
			
        elseif (parameters(5) == 1)
			disp('Starting Automated Segmentation 1 BM')
			
			if ~exist(gcpath,'dir')
				mkdir(gcpath);
			end

			AD_gc_script_1BM(BVpath,fpsave,gcpath,Nsteps,volNo-1)
			disp('Automated Segmentation Finished')
        end
    else
        disp('Segmentation file already exists.')
    end

%% Quickcheck
	try
		QuickCheckFinalHuman(strcat(filename, '_', num2str(volNo - 1)), BVpath);
	catch
		disp('Quick Check failed');
	end
    
    %% EN FACE CREATION
    if  parameters(5) == 4

        %load(fullfile(octapath,[fpsave,'_',num2str(volNo-1),'_DAV.mat']));
%         load(fullfile(octapath,[fpsave,'_',num2str(volNo-1),'_cplxOCTA.mat']));
        load(fullfile(gcpath,[fpsave,'_',num2str(volNo-1),'-INLseg.mat']));
        load(fullfile(gcpath,[fpsave,'_',num2str(volNo-1),'-ILMseg.mat']));
        load(fullfile(gcpath,[fpsave,'_',num2str(volNo-1),'-ONLseg.mat']));
%         load(fullfile(mcorrpath,[fpsave,'_',num2str(volNo-1),'    _mcorr.mat']));

        if ~exist(qcpath, 'dir')
            mkdir(qcpath);
		end
		
		% Initialize image buffers
		fund_all = zeros([size(cplxOCTA, 2), size(cplxOCTA, 3)]);
		fund_deep = fund_all;
		fund_sup = fund_all;
		
        for i = 1:size(cplxOCTA,2)
            for j = 1:size(cplxOCTA,3)
                fund_all(i,j) = mean(cplxOCTA(gcILM(i,j):gcONL(i,j)-20,i,j),1); %INL was +10
                fund_deep(i,j) = mean(cplxOCTA(gcINL(i,j):gcONL(i,j)-13,i,j),1); %ONL was  - 20
				
				% Attempt to help reduce cows
				if gcILM(i,j) > gcINL(i,j)
					gcINL(i,j) = gcINL(i,j)+15;
				end
				
                fund_sup(i,j) = mean(cplxOCTA(gcILM(i,j):gcINL(i,j),i,j),1);
            end
        end
        
%         V2 = abs(volume_mcorr);
        
        %Average volume_mcorr
%         if parameters(5) == 1
%             % do nothing
%         elseif parameters(5) == 2
%             v3(:,:,:,1) = V2(:,:,1:2:end);
%             v3(:,:,:,2) = V2(:,:,2:2:end);
%             V4 = squeeze(mean(v3,4));
%             volume = V4;
%         elseif parameters(5) == 4
%             v3(:,:,:,1) = V2(:,:,1:4:end);
%             v3(:,:,:,2) = V2(:,:,2:4:end);
%             v3(:,:,:,3) = V2(:,:,3:4:end);
%             v3(:,:,:,4) = V2(:,:,4:4:end);
%             volume = squeeze(mean(v3,4));
%         end
        
%        clear v3; clear V4; clear V2;         

% 		for i = 1:size(volume,2)
% 			for j = 1:size(volume,3)
% 				intensity_all(i,j) = mean(volume(gcILM(i,j):gcONL(i,j)-10,i,j),1); %INL was +10
% 				intensity_deep(i,j) = mean(volume(gcINL(i,j)-10:gcONL(i,j)-10,i,j),1); %ONL was  - 20
% 				intensity_sup(i,j) = mean(volume(gcILM(i,j):gcINL(i,j)-10,i,j),1);
% 				intensity_int(i,j) = mean(volume(round(gcINL(i,j)-5):round(gcINL(i,j))+5,i,j),1);
% 			end
% 		end

		% Save images
        imwrite((imadjust(mat2gray(fund_all))),fullfile(qcpath,[filename,'_',num2str(volNo-1),'_all.tif']));
        imwrite((imadjust(mat2gray(fund_sup))),fullfile(qcpath,[filename,'_',num2str(volNo-1),'_sup.tif']));
        imwrite((imadjust(mat2gray(fund_deep))),fullfile(qcpath,[filename,'_',num2str(volNo-1),'_deep.tif']));
%         imwrite((imadjust(mat2gray(fund_int))),fullfile(qcpath,[filename,'_',num2str(volNo-1),'_int.tif']));

        % Intensity fundus
% 		imwrite((imadjust(mat2gray(intensity_all))),fullfile(qcpath,[filename,'_',num2str(volNo-1),'_in_all.tif']));
% 		imwrite((imadjust(mat2gray(intensity_sup))),fullfile(qcpath,[filename,'_',num2str(volNo-1),'_in_sup.tif']));
% 		imwrite((imadjust(mat2gray(intensity_deep))),fullfile(qcpath,[filename,'_',num2str(volNo-1),'_in_deep.tif']));
% 		imwrite((imadjust(mat2gray(intensity_int))),fullfile(qcpath,[filename,'_',num2str(volNo-1),'_in_int.tif']));
          
    elseif (parameters(5) == 1 || parameters(5) == 2)
        try
            load(fullfile(mcorrpath,[fpsave,'_',num2str(volNo-1),'    _mcorr.mat']));
			load(fullfile(gcpath,[fpsave,'_',num2str(volNo-1),'-ONLseg.mat']));
%             load(fullfile(gcpath,[filename,'_',num2str(volNo-1),'-ILMseg.mat']));

            volume_mcorr = abs(volume_mcorr);

            for i = 1:size(volume_mcorr,2)
                for j = 1:size(volume_mcorr,3)
					fund_nfl(i, j) = mean(volume_mcorr(gcILM(i, j):gcILM(i, j)+6, i, j), 1);
%                      fund_all(i,j) = mean(volume_mcorr(gcONL(i,j)-12:gcONL(i,j)-7,i,j),1);
% 					fund_rpe(i,j) = mean(volume_mcorr(gcONL(i,j)-10:gcONL(i,j),i,j),1);
                end
            end
            

            % save data
            if ~exist(qcpath,'dir')
                mkdir(qcpath);
			end
			
% 			imwrite(imadjust(mat2gray(fund_nfl)),fullfile(qcpath,[filename,'_',num2str(volNo-1),'_NFL.tif']));
			imwrite(mat2gray(fund_all),fullfile(qcpath,[filename,'_',num2str(volNo-1),'_Cones.tif']));

%             imwrite(imadjust(mat2gray(fund_all)),fullfile(qcpath,[filename,'_',num2str(volNo-1),'_PR.tif']));
%           imwrite(imadjust(mat2gray(fund_RPE)),fullfile(qcpath,[filename,'_',num2str(volNo-1),'_RPE.tif']));

        catch
            warning('En Face Creation failed!')
        end
    end

    
end % End for loop

end % End function



