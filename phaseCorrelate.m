function volume_mcorr = phaseCorrelate(ref, volume)

% Initialize output
volume_mcorr = zeros(size(volume));

% We want to register sets of BM scans together, not individual B scans
for ii = 1:size(volume, 3)
    R = ifft2((fft2(ref).*conj(fft2(volume(:, :, ii)))) ./ abs(fft2(ref).*conj(fft2(volume(:, :, ii))))); % Phase Correlation equation
    R = imgaussfilt(R,2); % Gaussian Filter of R with sigma=2, same as imnoise() play with sigma depending on output (2 works well for Sup)
    [ro, co] = find(R == max(max(R))); % argmax(R)
    ro = ro-1;
    co = co-1;
    dx = co-size(m,2)*round(co/size(m,2)); % Centering phase shift
    dy = ro-size(m,1)*round(ro/size(m,1));

    volume_mcorr(:, :, ii) = imtranslate(volume(:, :, ii), [dx, dy]);
end

end