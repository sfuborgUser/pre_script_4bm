function AD_gc_script_4BM2(loadloc,filename,savepath,Nsteps,volNo)


if ~exist(savepath)
    mkdir(savepath)
end

load(fullfile(loadloc,[filename,'_',num2str(volNo),'-BV_it',num2str(Nsteps),'.mat']));
V2 = (2*32768*mat2gray(V))-32768;
V2 = double(V2);

%Load Acquisition Parameters
fn2 = strcat(filename,'.xml');
parameters  = getParameters(fn2);
numPoints   = parameters(1);


% V5 = 20*log10(mat2gray(V2));
V5 = V2;

% ONL
% slope = 1; noop=@(M) M; negate = @(M) -M;
%     surfDist1 = [150,10]; %was 150/50
%     surflines = [4,1]; numSurf = size(surflines,1);
%     instructions = {{[],[]},        [0],                   8, 4,            surfDist1,        2, noop;...
%         {[1,1]},        [0],                   4, 2,            zeros(0,2),       2, noop;...
%         {[2,1]},        [0],                   4, 2,            zeros(0,2),       2, noop;...
%         {[3,1]},        [0],                   1, 1,            zeros(0,2),       2, noop;...
%         };
% gc = graphCut3D_prior(V5, numSurf, slope, instructions, surflines,0);
% gcELM = squeeze(gc{1});
% % filter
% h = fspecial('disk', 30);
% gcONL = round(imfilter(gcELM,h,'replicate'));
% save(fullfile(savepath,[filename,'_',num2str(volNo),'-ONLseg.mat']),'gcONL','-v7.3');
% toc
% 
% 
% ILM
slope = 1; noop=@(M) M; negate = @(M) -M;
surfDist1 = [150,10]; %was 270/10 and noop for original ILM
surflines = [4,1];  numSurf = size(surflines,1);
instructions = {{[],[]},            [0],                 4, 2,          surfDist1,       5, noop;...   % ILM
    {[1,2]},            [0],                 4, 2,          zeros(0,2),      5, noop;...
    {[2,1]},            [0],                 2, 1,          zeros(0,2),      5, noop;...
    {[3,1]},            [0],                 1, 1,          zeros(0,2),      5, noop;...  % IO based on ILM
    };
gc = graphCut3D_prior(V5, numSurf, slope, instructions, surflines,0);
gcILM = squeeze(gc{1});
h = fspecial('disk',10);
gcILM = round(imfilter(gcILM,h,'replicate'));


gcINL = gcILM+15;
save(fullfile(savepath,[filename,'_',num2str(volNo),'-ILMseg.mat']),'gcILM','-v7.3');
save(fullfile(savepath,[filename,'_',num2str(volNo),'-INLseg.mat']),'gcINL','-v7.3');

% load(fullfile(savepath,[filename,'_',num2str(volNo),'-ONLseg.mat']));
% 
% mask
% [~, X, ~] = meshgrid(1:size(V5,2),1:size(V5,1),1:size(V5,3));
% tempvol = zeros([1 size(gcONL,1) size(gcONL,2)]); tempvol(1,:,:) = gcONL;
% tempvol= repmat(tempvol,[size(V5,1),1,1]);
% X = (X-3) > tempvol;
% BVvol2 = V5;
% BVvol2(find(X)) = 32768;
% % 2. Use ELM as reference and find three dark-to-bright interface
% slope = 1; noop=@(M) M; negate = @(M) -M;
% prior{1} = reshape(gcONL,[1 size(gcONL,1) size(gcONL,2)]);
% % surfDist1 = [150,50; 150, 50]; %was 35/10
% surfDist1 = [35,5; 35, 5]; %was 35/10
% surflines = [4,1];  numSurf = size(surflines,1);
% instructions = {{[],[],[]},                        [0,  0, 0],                 8, 4,          surfDist1,                5, negate;...
%     {[1,1],[1,2],[1,3]},               [0,  0, 0],                 2, 1,          surfDist1,      [5, 5, 5], negate;...
%     {[2,2]},                                  [0],                 2, 1,          zeros(0,2),               5, negate;...
%     {[3,1]},                                  [0],                 1, 1,          zeros(0,2),               5, negate;...
%     };
% gc = graphCut3D_prior(BVvol2, numSurf, slope, instructions, surflines,prior);
% gcINL = squeeze(gc{1});
% 
% % filter
% h = fspecial('disk', 25);
% gcINL = round(imfilter(gcINL,h,'replicate'));
% gcINL = gcINL - 40;
% save(fullfile(savepath,[filename,'_',num2str(volNo),'-INLseg.mat']),'gcINL','-v7.3');

% slope = 1; noop=@(M) M; negate = @(M) -M;
%     surfDist1 = [400,200]; %was 150/50
%     surflines = [4,1]; numSurf = size(surflines,1);
%     instructions = {{[],[]},        [0],                   8, 4,            surfDist1,        5, noop;...
%         {[1,1]},        [0],                   4, 2,            zeros(0,2),       5, noop;...
%         {[2,1]},        [0],                   2, 1,            zeros(0,2),       5, noop;...
%         {[3,1]},        [0],                   1, 1,            zeros(0,2),       5, noop;...
%         };
% gc = graphCut3D_prior(V5, numSurf, slope, instructions, surflines,0);
% gcELM = squeeze(gc{1});
% % filter
% h = fspecial('disk', 30);
% gcINL = round(imfilter(gcELM,h,'replicate'));
% gcINL = gcINL - 40;
% save(fullfile(savepath,[filename,'_',num2str(volNo),'-INLseg.mat']),'gcINL','-v7.3');
% % toc


end
%end



