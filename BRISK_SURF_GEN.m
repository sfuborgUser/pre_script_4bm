% This function uses a combination of BRISK and SURF feature identification
% to create a more robust feature-based rigid registration method. Adapted
% from a Mathworks tutorial page.

function [recovered_all, recovered_opl, recovered_sup, recovered_int, ...
    recovered_in_all, recovered_in_opl, recovered_in_sup, recovered_in_int] = ... 
    BRISK_SURF_GEN(original_all, distorted_all, distorted_opl, distorted_sup, distorted_int, ...
    distorted_in_all, distorted_in_opl, distorted_in_sup, distorted_in_int)

    MatchThreshold = 40;    % Works well. Increasing this increases the percent distance from a 'perfect match', i.e. more matches
    MaxRatio = 0.9;         % Works well. Increasing this increases the number of ambiguous matches, i.e. more matches

    ptsOriginalBRISK = detectBRISKFeatures(original_all, 'MinContrast', 0.01);
    ptsDistortedBRISK = detectBRISKFeatures(distorted_all, 'MinContrast', 0.01);

    ptsOriginalSURF = detectSURFFeatures(original_all);
    ptsDistortedSURF = detectSURFFeatures(distorted_all);

    [featuresOriginalFREAK, validPtsOriginalBRISK] = extractFeatures(original_all, ptsOriginalBRISK);
    [featuresDistortedFREAK, validPtsDistortedBRISK] = extractFeatures(distorted_all, ptsDistortedBRISK);

    [featuresOriginalSURF, validPtsOriginalSURF] = extractFeatures(original_all, ptsOriginalSURF);
    [featuresDistortedSURF, validPtsDistortedSURF] = extractFeatures(distorted_all, ptsDistortedSURF);

    indexPairsBRISK = matchFeatures(featuresOriginalFREAK, featuresDistortedFREAK, 'MatchThreshold', MatchThreshold, 'MaxRatio', MaxRatio, 'Unique', true);
    indexPairsSURF = matchFeatures(featuresOriginalSURF, featuresDistortedSURF);

    matchedOriginalBRISK = validPtsOriginalBRISK(indexPairsBRISK(:,1));
    matchedDistortedBRISK = validPtsDistortedBRISK(indexPairsBRISK(:,2));

    matchedOriginalSURF = validPtsOriginalSURF(indexPairsSURF(:,1));
    matchedDistortedSURF = validPtsDistortedSURF(indexPairsSURF(:,2));

    % figure, showMatchedFeatures(original, distorted, matchedOriginalBRISK, matchedDistortedBRISK);
    % title(strcat('BRISK ONLY: ', 'MatchThreshold = ', num2str(MatchThreshold), ', MaxRatio = ', num2str(MaxRatio)))

    matchedOriginalXY = [matchedOriginalSURF.Location; matchedOriginalBRISK.Location];
    matchedDistortedXY = [matchedDistortedSURF.Location; matchedDistortedBRISK.Location];
    
    % If there aren't enough matches, discard the strip
    if (size(matchedOriginalXY,1) < 3) || (size(matchedDistortedXY,1) < 3)
        recovered_all = zeros(size(original_all));
        recovered_opl = zeros(size(original_all));
        recovered_sup = zeros(size(original_all));
        recovered_int = zeros(size(original_all));
        recovered_in_all = zeros(size(original_all));
        recovered_in_opl = zeros(size(original_all));
        recovered_in_sup = zeros(size(original_all));
        recovered_in_int = zeros(size(original_all));
        
        return
    end
        

    [tformTotal, inlierDistortedXY, inlierOriginalXY] = estimateGeometricTransform(matchedDistortedXY, matchedOriginalXY, 'affine', 'MaxDistance', 5.0, 'MaxNumTrials', 1000000);
    
    % If the maximum Y difference between any of the matching keypoints is
    % greater than a threshold (50 seems to be a good number, may need
    % refinements) discard the strip.
    % UPDATE: This metric doesn't work for large images, and maybe I was
    % getting rid of good medium images too. Re-evaluate
    if (max(abs(inlierDistortedXY(:,2) - inlierOriginalXY(:,2)))) > 75
        recovered_all = zeros(size(original_all));
        recovered_opl = zeros(size(original_all));
        recovered_sup = zeros(size(original_all));
        recovered_int = zeros(size(original_all));
        recovered_in_all = zeros(size(original_all));
        recovered_in_opl = zeros(size(original_all));
        recovered_in_sup = zeros(size(original_all));
        recovered_in_int = zeros(size(original_all));
        
        return
    end
    
%     figure, showMatchedFeatures(original_all, distorted_all, inlierOriginalXY, inlierDistortedXY);
%     title(strcat('BRISK + SURF Points: ', ' MatchThreshold = ', num2str(MatchThreshold), ', MaxRatio = ', num2str(MaxRatio)))
%     
%     figure, showMatchedFeatures(original_all, distorted_all, matchedOriginalBRISK, matchedDistortedBRISK);
%     title(strcat('BRISK Points: ', ' MatchThreshold = ', num2str(MatchThreshold), ', MaxRatio = ', num2str(MaxRatio)))
%     
%     figure, showMatchedFeatures(original_all, distorted_all, matchedOriginalSURF, matchedDistortedSURF);
%     title(strcat('SURF Points: ', ' MatchThreshold = ', num2str(MatchThreshold), ', MaxRatio = ', num2str(MaxRatio)))

    OutputView = imref2d(size(original_all));
    recovered_all = imwarp(distorted_all, tformTotal, 'OutputView', OutputView);
    recovered_opl = imwarp(distorted_opl, tformTotal, 'OutputView', OutputView);
    recovered_sup = imwarp(distorted_sup, tformTotal, 'OutputView', OutputView);
    recovered_int = imwarp(distorted_int, tformTotal, 'OutputView', OutputView);
    recovered_in_all = imwarp(distorted_in_all, tformTotal, 'OutputView', OutputView);
    recovered_in_opl = imwarp(distorted_in_opl, tformTotal, 'OutputView', OutputView);
    recovered_in_sup = imwarp(distorted_in_sup, tformTotal, 'OutputView', OutputView);
    recovered_in_int = imwarp(distorted_in_int, tformTotal, 'OutputView', OutputView);

%     figure, imshowpair(original_all, recovered_all);
    
    
    % close all
    
    % Best idea at the moment for finding badly registered strips: use a
    % variance calculation of some sort. Vertical and horizontal
    % differences don't look promising (although haven't seen the values
    % for a badly registered strip yet
    
    % Nevermind! It may indeed work lol. Set the threshold to maxY = 75 
    
    % figure, imshowpair(original_all, recovered_all)
end