% function [imout_all, imout_opl, imout_sup, imout_int, imout_in_all, imout_in_opl, imout_in_sup, imout_in_int] = sv_strip_templateless_GEN(loadloc,filename,thresh,sizeStrips)
function [imout_all, imout_opl, imout_sup, imout_int] = sv_strip_templateless_GEN(loadloc,filename,thresh,sizeStrips)

% SV_STRIP  Creates microsaccade-free strips from OCTA images.
append_opl = '_deep.tif';
append_sup = '_sup.tif';
append_int = '_int.tif';
append_all = '_all.tif';

% append_in_all = '_all.tif';
% append_in_opl = '_deep.tif';
% append_in_sup = '_sup.tif';
% append_in_int = '_int.tif';

imraw_all = double(imread(fullfile(loadloc,[filename(1:end-8),append_all])));
imraw_opl = double(imread(fullfile(loadloc,[filename(1:end-8),append_opl])));
imraw_sup = double(imread(fullfile(loadloc,[filename(1:end-8),append_sup])));
imraw_int = double(imread(fullfile(loadloc,[filename(1:end-8),append_int])));

% imraw_all = double(imread(fullfile(loadloc,[filename(1:end-11),append_all])));
% imraw_opl = double(imread(fullfile(loadloc,[filename(1:end-11),append_opl])));
% imraw_sup = double(imread(fullfile(loadloc,[filename(1:end-11),append_sup])));
% imraw_int = double(imread(fullfile(loadloc,[filename(1:end-11),append_int])));

% imraw_in_all = double(imread(fullfile(loadloc,[filename(1:end-8),append_in_all])));
% imraw_in_opl = double(imread(fullfile(loadloc,[filename(1:end-8),append_in_opl])));
% imraw_in_sup = double(imread(fullfile(loadloc,[filename(1:end-8),append_in_sup])));
% imraw_in_int = double(imread(fullfile(loadloc,[filename(1:end-8),append_in_int])));


imraw_all = imraw_all(:,:,1); 
imraw_opl = imraw_opl(:,:,1); 
imraw_sup = imraw_sup(:,:,1);
imraw_int = imraw_int(:,:,1);

% imraw_in_all = imraw_in_all(:,:,1); 
% imraw_in_opl = imraw_in_opl(:,:,1); 
% imraw_in_sup = imraw_in_sup(:,:,1);
% imraw_in_int = imraw_in_int(:,:,1);

%% Projection artifact removal and such
%figure, imshow(mat2gray(imraw_opl))
imraw_opl = mat2gray(imraw_opl.*mat2gray((mat2gray(imraw_all)-mat2gray(imraw_sup)))); % The normal way.
% imraw_in_opl = mat2gray(imraw_in_opl.*mat2gray((mat2gray(imraw_in_all)-mat2gray(imraw_in_sup)))); % Don't know if I should be doing this for the intensity as well
%imraw_opl = mat2gray(imraw_opl.*mat2gray((mat2gray(imraw)-mat2gray(imraw_sup))));
%imraw_opl2 = mat2gray(imraw_opl).*(1-mat2gray(imraw_sup)); % The other way. Causes black superficial vessels, but works better for some subjects
imraw_all = mat2gray(imraw_all);
imraw_sup = mat2gray(imraw_sup);
imraw_int = mat2gray(imraw_int);

% imraw_in_all = mat2gray(imraw_in_all);
% imraw_in_sup = mat2gray(imraw_in_sup);
% imraw_in_int = mat2gray(imraw_in_int);
%figure, imshow(imraw_opl)
%figure, imshow(imraw_opl2)
%close all
% Before, I had it so that I was cropping both x and y dimensions after
% finding the peaks. This is a mistake - because after cropping, the locs
% actually change (when cropping the x/cols dimension). So now, I crop both 
% dimensions by 25 before finding the peaks.
%
% Below is for 200khz data (where the first 25 columns have that weird
% artifact

%% Comment this out if working with AO data
imraw_all = imraw_all(26:end, 26:end); imraw_opl = imraw_opl(26:end, 26:end); imraw_sup = imraw_sup(26:end, 26:end); imraw_int = imraw_int(26:end, 26:end);
% imraw_in_all = imraw_in_all(26:end, 26:end); imraw_in_opl = imraw_in_opl(26:end, 26:end); imraw_in_sup = imraw_in_sup(26:end, 26:end); imraw_in_int = imraw_in_int(26:end, 26:end);

%% Peak Detection
% Below is for old 100khz data
% imraw = imraw(26:end-25, :); imraw_opl = imraw_opl(26:end-25, :); imraw_sup = imraw_sup(26:end-25, :);
%
avg_col = mean(imraw_all((size(imraw_all,1)/2)-50:(size(imraw_all,1)/2)+50,:),1); 
[pks,locs,widths,~] = findpeaks(avg_col,'MinPeakDistance', 20, 'MinPeakWidth', 3, 'MinPeakHeight', (mean(avg_col)/2)*4.1); % previously distance = 15, width = 3, height -> *3.0
% Parameters that worked well in general: MinPD = 15, MinPW = 3.7, MaxPW =
% 10, MinPH = (mean(avg_col)/2)*3.7)

% [pks,locs,widths,~] = findpeaks(avg_col,'MinPeakProminence', 0.15); 
% figure, imshow(imraw_all)
% pks
% widths
% locs

% % Draw the suspected motion artifacts onto the image
% for ii = 1:size(pks,2)
%     hold on
%     line([locs(ii) locs(ii)], [1 375], 'Color', 'r', 'LineWidth', 1.5)
% end
% close all


% % Blink detection - assumes a maximum of one blink per frame
% sumcol = sum(imraw,1);
% threshcol = mean(sumcol)/2;
% blinklocs = find(sumcol < threshcol);
% if blinklocs > 0 % Add the blink borders to the locs and widths arrays if a blink is detected
%     blinkmin = blinklocs(1) - 8;
%     blinkmax = blinklocs(end) + 8;
%     imraw(:, blinkmin:blinkmax) = 0; % Zero the region between the blinks
%     imraw_opl(:, blinkmin:blinkmax) = 0;
%     imraw_sup(:, blinkmin:blinkmax) = 0;
%     locsnew = locs;
%     locsnew(size(locsnew,2)+1) = blinkmin;
%     locsnew(size(locsnew,2)+1) = blinkmax;
%     locsnew = sort(locsnew, 'ascend');
%     widthsnew = zeros(1, size(widths,2) + 2);
%     for ii = 1:size(locs,2) % This will update the widths array so that the locations of blinks have widths of zero
%         ind = find(locsnew == locs(ii));
%         widthsnew(ind) = widths(ii);
%     end
%     locs = locsnew;
%     widths = widthsnew;
% end

% Morgan took out the notch filter
%%%%% Bad Morgan

%% Imadjust

imraw_all = notchfilterJing(imraw_all);
imraw_all = imadjust(imraw_all); 

imraw_opl = notchfilterJing(imraw_opl);
imraw_opl = imadjust(imraw_opl);

imraw_sup = notchfilterJing(imraw_sup);
imraw_sup = imadjust(imraw_sup); 

imraw_int = notchfilterJing(imraw_int);
imraw_int = imadjust(imraw_int); 

% imraw_in_all = notchfilterJing(imraw_in_all);
% imraw_in_all = imadjust(imraw_in_all); 
% 
% imraw_in_opl = notchfilterJing(imraw_in_opl);
% imraw_in_opl = imadjust(imraw_in_opl);
% 
% imraw_in_sup = notchfilterJing(imraw_in_sup);
% imraw_in_sup = imadjust(imraw_in_sup); 
% 
% imraw_in_int = notchfilterJing(imraw_in_int);
% imraw_in_int = imadjust(imraw_in_int); 

imout_all = zeros([size(imraw_all),size(locs,2) + 1]); imout_opl = zeros([size(imraw_all),size(locs,2) + 1]); imout_sup = zeros([size(imraw_all),size(locs,2) + 1]); imout_int = zeros([size(imraw_all),size(locs,2) + 1]);
% imout_in_all = zeros([size(imraw_all),size(locs,2) + 1]); imout_in_opl = zeros([size(imraw_all),size(locs,2) + 1]); imout_in_sup = zeros([size(imraw_all),size(locs,2) + 1]); imout_in_int = zeros([size(imraw_all),size(locs,2) + 1]);

%%%%% Good Morgan

% imraw = (imraw); imraw = imadjust(mat2gray(imraw)); %imraw = imraw(1:end-10,:);
% imraw_opl = (imraw_opl); imraw_opl = imadjust(mat2gray(imraw_opl)); %imraw_opl = imraw_opl(1:end-10,:);
% imraw_sup = (imraw_sup); imraw_sup = imadjust(mat2gray(imraw_sup)); %imraw_sup = imraw_sup(1:end-10,:);
% imout = zeros([size(imraw),size(locs,2) + 1]); imout_opl = zeros([size(imraw),size(locs,2) + 1]); imout_sup = zeros([size(imraw),size(locs,2) + 1]);
%  figure, imshow(imraw_opl)

%Projection Artefact Removal
% imraw_opl = imadjust(mat2gray(imraw_opl.*mat2gray(imraw-imraw_sup)));

%  figure, imshow(imraw_opl)
%  close all

% %% Intial Registration of Strips
% [output, ~] = dftregistration(fft2(double(im1)*255),fft2(double(imraw)*255),100);
% fedge  =find(mean(imraw),1);
% [~,I] = (sort((abs(diff([0 mean(imraw) 0]))),'descend'));
% I = I-1;
% if 0 > fedge + round(output(4))
%     imraw = circshift(circshift(imraw,round(output(4)),2),round(output(3)),1);
%     imraw_opl = circshift(circshift(imraw_opl,round(output(4)),2),round(output(3)),1);
%     imraw_sup = circshift(circshift(imraw_sup,round(output(4)),2),round(output(3)),1);
%     
%     imraw(:,size(imraw,2)-abs(fedge + round(output(4))):size(imraw,2)) = 0;
%     imraw_opl(:,size(imraw,2)-abs(fedge + round(output(4))):size(imraw,2)) = 0;
%     imraw_sup(:,size(imraw,2)-abs(fedge + round(output(4))):size(imraw,2)) = 0;
% elseif round(output(4)) == 0 || (max(I(1:2)) + abs(round(output(4)))) < size(imraw,2)
%     imraw = circshift(circshift(imraw,round(output(4)),2),round(output(3)),1);
%     imraw_opl = circshift(circshift(imraw_opl,round(output(4)),2),round(output(3)),1);
%     imraw_sup = circshift(circshift(imraw_sup,round(output(4)),2),round(output(3)),1);
% else
%     imraw = circshift(circshift(imraw,round(output(4)),2),round(output(3)),1);
%     imraw_opl = circshift(circshift(imraw_opl,round(output(4)),2),round(output(3)),1);
%     imraw_sup = circshift(circshift(imraw_sup,round(output(4)),2),round(output(3)),1);
%     
%     imraw(:,1:abs(size(imraw,2)-abs(max(I(1:2)) + abs(round(output(4)))))) = 0;
%     imraw_opl(:,1:abs(size(imraw,2)-abs(max(I(1:2)) + abs(round(output(4)))))) = 0;
%     imraw_sup(:,1:abs(size(imraw,2)-abs(max(I(1:2)) + abs(round(output(4)))))) = 0;
% end
% if 0 < round(output(3))
%     imraw(1:abs(round(output(3))),:) = 0;
%     imraw_opl(1:abs(round(output(3))),:) = 0;
%     imraw_sup(1:abs(round(output(3))),:) = 0;
% elseif round(output(3)) < 0
%     imraw(size(imraw,1)-abs(round(output(3))):end,:) = 0;
%     imraw_opl(size(imraw,1)-abs(round(output(3))):end,:) = 0;
%     imraw_sup(size(imraw,1)-abs(round(output(3))):end,:) = 0;
% end

%% Strip creation

% %%
% %update motion locations
% locs = locs + round(output(4));
% pks(locs < 1) = [];locs(locs < 1) = [];widths(locs < 1) = [];
% pks(locs > size(imraw,2)) = [];locs(locs > size(imraw,2)) = [];widths(locs > size(imraw,2)) = [];

if isempty(locs)
    imout_all = imraw_all;
    imout_opl = imraw_opl;
    imout_sup = imraw_sup;
    imout_int = imraw_int;
    
%     imout_in_all = imraw_in_all;
%     imout_in_opl = imraw_in_opl;
%     imout_in_sup = imraw_in_sup;
%     imout_in_int = imraw_in_int;
    %chop image into every 'sizeStrips' pixels
%     numStrips = size(imraw,2)/sizeStrips;
%     for ind = 1:numStrips
%         imout(:,((ind-1)*sizeStrips)+1:ind*sizeStrips, size(imout,3) + 1) = imraw(1:(size(imraw,1)),((ind-1)*sizeStrips)+1:ind*sizeStrips); %all
%         imout_opl(:,((ind-1)*sizeStrips)+1:ind*sizeStrips,size(imout_opl,3) + 1) = imraw_opl(1:(size(imraw,1)),((ind-1)*sizeStrips)+1:ind*sizeStrips); %OPL        
%         imout_sup(:,((ind-1)*sizeStrips)+1:ind*sizeStrips,size(imout_sup,3) + 1) = imraw_sup(1:(size(imraw,1)),((ind-1)*sizeStrips)+1:ind*sizeStrips); %superficial
%     end
else
    % Break into strips
    for j = 1:size(locs,2)
        %For one peak
        if j == 1
            %if size(1:floor(locs(j)-widths(j)-2),2) > thresh % if the strips are greater than the threshold
            if size(locs,2) > 1 % if there is more than one peak (ie. more than 2 strips)
                if size(1:floor(locs(j)-widths(j)-2),2) > thresh
                    imout_all(:,1:floor(locs(j)-widths(j)-2),j) = imraw_all(:,1:floor(locs(j)-widths(j)-2));
                    imout_opl(:,1:floor(locs(j)-widths(j)-2),j) = imraw_opl(:,1:floor(locs(j)-widths(j)-2));
                    imout_sup(:,1:floor(locs(j)-widths(j)-2),j) = imraw_sup(:,1:floor(locs(j)-widths(j)-2));
                    imout_int(:,1:floor(locs(j)-widths(j)-2),j) = imraw_int(:,1:floor(locs(j)-widths(j)-2));
                    
%                     imout_in_all(:,1:floor(locs(j)-widths(j)-2),j) = imraw_in_all(:,1:floor(locs(j)-widths(j)-2));
%                     imout_in_opl(:,1:floor(locs(j)-widths(j)-2),j) = imraw_in_opl(:,1:floor(locs(j)-widths(j)-2));
%                     imout_in_sup(:,1:floor(locs(j)-widths(j)-2),j) = imraw_in_sup(:,1:floor(locs(j)-widths(j)-2));
%                     imout_in_int(:,1:floor(locs(j)-widths(j)-2),j) = imraw_in_int(:,1:floor(locs(j)-widths(j)-2));
                end
            else % if there are only 2 strips (ie one peak)
                if size(1:floor(locs(j)-widths(j)-2),2) > thresh
                    imout_all(:,1:floor(locs(j)-widths(j)-2),j) = imraw_all(:,1:floor(locs(j)-widths(j)-2));
                    imout_opl(:,1:floor(locs(j)-widths(j)-2),j) = imraw_opl(:,1:floor(locs(j)-widths(j)-2));
                    imout_sup(:,1:floor(locs(j)-widths(j)-2),j) = imraw_sup(:,1:floor(locs(j)-widths(j)-2));
                    imout_int(:,1:floor(locs(j)-widths(j)-2),j) = imraw_int(:,1:floor(locs(j)-widths(j)-2));
                    
%                     imout_in_all(:,1:floor(locs(j)-widths(j)-2),j) = imraw_in_all(:,1:floor(locs(j)-widths(j)-2));
%                     imout_in_opl(:,1:floor(locs(j)-widths(j)-2),j) = imraw_in_opl(:,1:floor(locs(j)-widths(j)-2));
%                     imout_in_sup(:,1:floor(locs(j)-widths(j)-2),j) = imraw_in_sup(:,1:floor(locs(j)-widths(j)-2));
%                     imout_in_int(:,1:floor(locs(j)-widths(j)-2),j) = imraw_in_int(:,1:floor(locs(j)-widths(j)-2));
                end
                if size(floor(locs(j)+widths(j)+2):(size(imraw_all,1)),2) > thresh %Check that the second strip is more than the threshold
                    imout_all(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_all(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
                    imout_opl(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_opl(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
                    imout_sup(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_sup(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
                    imout_int(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_int(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
                    
%                     imout_in_all(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_in_all(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
%                     imout_in_opl(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_in_opl(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
%                     imout_in_sup(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_in_sup(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
%                     imout_in_int(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_in_int(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
                end
            end
            
           %For the last peak
        elseif j == size(locs,2)          
            if (floor(locs(j)-widths(j)-2) - floor(locs(j-1)+widths(j-1)/2)) > thresh
                imout_all(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_all(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
                imout_opl(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_opl(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
                imout_sup(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_sup(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
                imout_int(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_int(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
                
%                 imout_in_all(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_in_all(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
%                 imout_in_opl(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_in_opl(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
%                 imout_in_sup(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_in_sup(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
%                 imout_in_int(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_in_int(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
            end
            if ((size(imraw_all,2))-floor(locs(j)+widths(j)+2)) > thresh
                imout_all(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_all(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
                imout_opl(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_opl(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
                imout_sup(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_sup(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
                imout_int(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_int(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
                
%                 imout_in_all(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_in_all(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
%                 imout_in_opl(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_in_opl(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
%                 imout_in_sup(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_in_sup(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
%                 imout_in_int(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)),j+1) = imraw_in_int(:,floor(locs(j)+widths(j)+2):(size(imraw_all,1)));
            end
            %For the middle Peaks
        else
            if (floor(locs(j)-widths(j)-2) - floor(locs(j-1)+widths(j-1)/2)) > thresh
                imout_all(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_all(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
                imout_opl(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_opl(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
                imout_sup(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_sup(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
                imout_int(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_int(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
                
%                 imout_in_all(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_in_all(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
%                 imout_in_opl(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_in_opl(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
%                 imout_in_sup(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_in_sup(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
%                 imout_in_int(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2),j) = imraw_in_int(:,floor(locs(j-1)+widths(j-1)):floor(locs(j)-widths(j)-2));
            end
        end
    end
end
end
