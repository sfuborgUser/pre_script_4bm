function ProcdData = proCplxAOOCT(fn,options)

%Load Acquisition Parameters
%Load Acquisition Parameters
if strcmp(fn(end-5),'_')
    fn2 = strcat(fn(1:end-6),'.xml');
else
    fn2 = strcat(fn(1:end-4),'.xml');
end
parameters  = getParameters(fn2);
numPoints   = parameters(1);
numAscans   = parameters(2);
numBscans   = parameters(3);
numCscans     = parameters(4);
numMscans     = parameters(5);

LUT           = options.rescaleParam;
scanType      = options.scanType;
calSigFilter  = options.calSigFilter;
dispMaxOrder  = options.dispMaxOrder;
coeffRange    = options.coeffRange;

% dispMaxOrder    = 5;
% coeffRange      = 30;

%%% Unp file open %%%
fid = fopen(fn);
volNum = 1;
 
%%% Set reference A-scan & dispersion coefficients %%%
ref_Frame     = 200;
fseek(fid,2*numPoints*numAscans*(ref_Frame-1),-1);
ref_RawData  = fread(fid,[numPoints,numAscans], 'uint16');
ref_FFTData  = fft(hilbert(ref_RawData));
ref_Aline    = ref_FFTData(:,end/2);

offSetIdx  = 15;
diffPhases = estPhaseDiff(ref_Aline, ref_FFTData, offSetIdx);
ref_Img = reSamplingWithDiffPhase(ref_FFTData, LUT, diffPhases, offSetIdx);

ref_img_FPNSub     = ref_Img...
        - (repmat(median(real(ref_Img),2), [1,size(ref_Img,2)])...
        +1j.*repmat(median(imag(ref_Img),2), [1,size(ref_Img,2)]));

ref_Img_HamWin     = ref_img_FPNSub.*repmat(hann(size(ref_img_FPNSub,1)),[1 size(ref_img_FPNSub,2)]);
depths             = [50, 250];
% depths				= [50 512];
dispCoeffs         = setDispCoeff(ref_Img_HamWin,depths,dispMaxOrder, coeffRange);
% % 
% ref_img_DispComp   = compDisPhase(ref_Img_HamWin,dispMaxOrder,dispCoeffs);
% ref_FFT = fft(ref_img_DispComp);
% figure
% ref_OCT = 20.*log10(abs(ref_FFT(20:250,:)));
% imagesc(imadjust(mat2gray(ref_OCT))); colormap(gray)
% 


for FrameNum = 1:numBscans  
	%%% Load raw spectrum %%%
    fseek(fid,2*numPoints*numAscans*(FrameNum-1),-1);
    rawData = fread(fid,[numPoints,numAscans], 'uint16');
    fftData = fft(hilbert(rawData));
    
    idx = rem(FrameNum,numMscans);
    if idx == 1
        ref_Aline = fftData(:,end/2);
    end 
    %%% Estimating spectral-shift amount in sub-pixel %%%
    diffPhases = estPhaseDiff(ref_Aline, fftData, offSetIdx);
    img        = reSamplingWithDiffPhase(fftData, LUT, diffPhases, offSetIdx);

    %%% DC & Fixed pattern noise remove process through median filtering %%%
    img_FPNSub    = img...
        - (repmat(median(real(img),2), [1,size(img,2)])...
        +1j.*repmat(median(imag(img),2), [1,size(img,2)]));

    %%% Windowing process %%%
    img_HamWin    = img_FPNSub.*repmat(hann(size(img_FPNSub,1)),[1 size(img_FPNSub,2)]);
    
    
    %%% Dispersion compensation with the estimated dispersion coefficients %%%
    img_DispComp  = compDisPhase(img_HamWin,dispMaxOrder,dispCoeffs);
    
    %%% FFT %%%
    img_FFT                      = fft(img_DispComp);
%     ProcdData(:,:,FrameNum)      = img_FFT(30:310,:);
	ProcdData(:,:,FrameNum)      = img_FFT(30:310,:);
    %fprintf('OCT volume process: %d\n', FrameNum);
end

    
if scanType == 1
    ProcdData(:,:,2:2:end) = fliplr(ProcdData(:,:,2:2:end));
end

fclose(fid);



