function fig = QuickCheckFinalHuman(fn,fp)

% addpath('/ensc/IMAGEBORG/DATA/ECC/scripts/final')

if  ~contains(fn,'2011') ==0 % || ~contains(fn,'2012') ==0
    dim(2) = 405;
else
    dim(2) =400;
end
dim(1) = 1024;  
dim(3) = 400;


% Load Files
load(fullfile(fp,[fn,'-BV_it10.mat']));
% load(fullfile(fp, strcat(fn, '    _mcorr.mat')));

%% define which frame to display
% Number of frames
numFr = size(V, 3);
st = round(numFr / 16);
frameN_B = 1:st:numFr;
%frameN_B = [15,40,65,90,115,140,165,190,215,240,265,290,315,340,365,390];


fseg = strrep(fp,'BVsmooth','Segment_gc');
% if exist(fseg)
%     load(fullfile(fseg,[fn,'_final.mat']));
%     fin = 1;
% else
%     fseg = strrep(fp,'BVsmooth','Segment_gc');
try 
	load(fullfile(fseg,[fn,'-ONLseg.mat']));
catch
end

try 
	load(fullfile(fseg,[fn,'-ILMseg.mat']));
catch
end

try
	load(fullfile(fseg,[fn,'-INLseg.mat']));
catch
end

fin = 0;
% end
% segfinal = permute(segfinal,[1 3 2]);

%% B scans

if fin == 1
    [~,b] =max(mean(squeeze(mean(V,3)),2));
    if b-450 < 1
        beginb = 1;
    else
        beginb = b-450;
    end
    if (b+ 250 > size(V,1))
        endb = size(V, 1);
    else
        endb = b + 250;
    end
    for i = 1:16
        frame = frameN_B(i);
        h(i) = subplot(4, 4, i);
        imagesc(V(beginb:endb, :, frame));
        seg = segfinal(:, :, frame);
        [x1, y1] = find(seg == 11);
        [x2, y2] = find(seg == 12);
        [x3, y3] = find(seg == 13);
        [x4, y4] = find(seg == 14);
        hold on
        plot(y1, x1 - beginb, 'm', 'Linewidth', 3);
        plot(y2, x2 - beginb, 'y', 'Linewidth', 3);
        plot(y3, x3 - beginb, 'g', 'Linewidth', 3);
        plot(y4, x4 - beginb, 'c', 'Linewidth', 3);
        hold off
        colormap(gray);
        axis off
    end
else
    [~,b] = max(mean(squeeze(mean(V,3)),2));
    if (b - 150 < 1)
        beginb = 1;
    else
        beginb = b-150;
    end
    if (b + 50 > size(V,1))
        endb = size(V,1);
    else
        endb = b + 50;
	end
	
	for i = 1:size(frameN_B, 2)
        frame = frameN_B(i);
        h(i) = subplot(4, 4, i);
        imagesc(imadjust(mat2gray(V(:, :, frame))));
%         seg = segfinal(:,:,frame);
%         [x1,y1] = find(seg==11);
%         [x2,y2] = find(seg==12);
%         [x3,y3] = find(seg==13);
%         [x4,y4] = find(seg==14);
        hold on
		try 
        plot(1:400, gcILM(:, frame), 'm', 'Linewidth', 1);
		catch
		end
		
		try 
        plot(1:400, gcINL(:, frame) + 5, 'y', 'Linewidth', 1);
		catch
		end
%         plot(y3,x3-beginb,'g','Linewidth',3)
		try
        plot(1:400, gcONL(:, frame)-10, 'c', 'Linewidth', 1);
		catch
		end
		
        hold off
        colormap(gray);
        axis off
    end    
    
% else 
%     [~,b] =max(mean(squeeze(mean(V,3)),2));
%     if b-200 < 1
%         beginb = 1;
%     else
%         beginb = b-200;
%     end
%     if b+50 > size(V,1)
%         endb = size(V,1);
%     else
%         endb = b+50;
%     end
%     for i = 1:16
%         frame = frameN_B(i);
%         h(i) = subplot(4,4,i);
%         imagesc(adapthisteq(imadjust(mat2gray(V(beginb:endb,:,frame*2)))));
% %         seg = segfinal(:,:,frame);
% %         [x1,y1] = find(seg==11);
% %         [x2,y2] = find(seg==12);
% %         [x3,y3] = find(seg==13);
% %         [x4,y4] = find(seg==14);
%         hold on
% %         plot(1:400,gcILM(:,frame)-beginb,'m','Linewidth',3)
% %         plot(1:400,gcINL(:,frame)-beginb+15,'y','Linewidth',3)
% %         plot(y3,x3-beginb,'g','Linewidth',3)
%         plot(1:400,gcONL(:,frame)-beginb-20,'c','Linewidth',3)
%         hold off
%         colormap(gray)
%         axis off
%     end    
end

%You have access to all handle graphics object properties for each axis through the
%array h. You can set the position of individual axes using the get and set commands
get(h(1),'Position');
set(h(1),'Position',[.0 .75 .25 .25]);
get(h(2),'Position');
set(h(2),'Position',[.25 .75 .25 .25]);
get(h(3),'Position');
set(h(3),'Position',[.5 .75 .25 .25]);
get(h(4),'Position');
set(h(4),'Position',[.75 .75 .25 .25]);
get(h(5),'Position');
set(h(5),'Position',[.0 .5 .25 .25]);
get(h(6),'Position');
set(h(6),'Position',[.25 .5 .25 .25]);
get(h(7),'Position');
set(h(7),'Position',[.5 .5 .25 .25]);
get(h(8),'Position');
set(h(8),'Position',[.75 .5 .25 .25]);
get(h(9),'Position');
set(h(9),'Position',[.0 .25 .25 .25]);
get(h(10),'Position');
set(h(10),'Position',[.25 .25 .25 .25]);
get(h(11),'Position');
set(h(11),'Position',[.5 .25 .25 .25]);
get(h(12),'Position');
set(h(12),'Position',[.75 .25 .25 .25]);
get(h(13),'Position');
set(h(13),'Position',[.0 .0 .25 .25]);
get(h(14),'Position');
set(h(14),'Position',[.25 .0 .25 .25]);
get(h(15),'Position');
set(h(15),'Position',[.5 .0 .25 .25]);
get(h(16),'Position');
set(h(16),'Position',[.75 .0 .25 .25]);
%
%
% %% Slow scan
% options.bScanDir =1;
%
% for i= 1:6
%     frame = frameN_slow(i);
%     options.stFrame = frame-3;
%     test =mean(ProcRawSS(fn,5,options,fp,fmcorr),3);
%     if ~contains(fn,'2017') ==0 && contains(fn,'H182') ==0
%         slow_scan(i,:,:) = imresize(test(:,1:2:end),[size(test,1) size(test,2)]);
%     else
%         slow_scan(i,:,:) = test;
%     end
% end
%
% %% Create fundus
%
% h = fspecial('gaussian',[3 3]);
% fundus = procFundus(fn,[dim(2) dim(1) dim(3)]);
% if  ~contains(fn,'2017') ==0 && contains(fn,'H182') ==0
%     fundus(1:2:end,:) = flipdim(fundus(1:2:end,:),2);
% end
% fundus = 20*log10(imfilter(fundus,h));
%
% %% Display fundus
%
% figure;
% set(gcf,'color',[0,0,0]);
% axis off;axis equal;
% subplot('position',[0.001,0.5,0.498,0.498]);
% if contains(fn,'.unp')
%     imagesc(flipdim(flipdim(rot90(fundus),2),1));axis off;
% else
%     imagesc(flipdim(fundus,2));axis off;
% end
% hold on
% for i =1:1:6
%     line([1:400],frameN_B(i));
%     line(frameN_slow(i), [1:400]);
% end
%
% text(50,100,strcat(num2str(fn)),'Interpreter','none','Color','y','fontsize',10,'fontweight','bold');
% text(50,350,'fundus','Interpreter','none','Color','y','fontsize',10,'fontweight','bold');
%
%
% %% Display on the top right (4 Bscans)
% Num_Ho = 2;
% Num_Ve = 2;
% interval = 0.001;
% Wscale = (0.5-interval*Num_Ho)/Num_Ho;
% Hscale = (0.5-interval*Num_Ve)/Num_Ve;
% for i = 1:4
%     if i<3
%         boundaryLeft = 0.5+(interval +Wscale)*(i-1);
%         boundaryBottom = interval;
%     else
%         boundaryLeft = 0.5+(interval +Wscale)*(i-2-1);
%         boundaryBottom = interval+Hscale;
%     end
%     subplot('position',[boundaryLeft,1-Wscale-boundaryBottom,Wscale,Hscale]);
%     imagesc((squeeze(flipdim(B_scan(i,:,:),2))));axis off;
%     ylim([250 1014])
%     set(gca,'ydir','normal');
%     text(boundaryLeft*100,(interval+Hscale)*880,['Bscan',num2str(frameN_B(i))],'Color','y','fontsize',10,'fontweight','bold');
% end
%
% %% Display on the bottom left (4 slow scans)
% for i = 1:4
%     if i<3
%         boundaryLeft = (interval +Wscale)*(i-1);
%         boundaryBottom = 0.5+interval;
%     else
%         boundaryLeft = (interval +Wscale)*(i-2-1);
%         boundaryBottom = .5+interval+Hscale;
%     end
%     subplot('position',[boundaryLeft,1-Wscale-boundaryBottom,Wscale,Hscale]);
%     imagesc(squeeze(flipdim(slow_scan(i,:,:),2)));axis off;
%     ylim([250 1014])
%     set(gca,'ydir','normal');
%     text(boundaryLeft*100,(0.5+interval+Hscale)*290,['SlowScan',num2str(frameN_slow(i))],'Color','y','fontsize',10,'fontweight','bold');
% end
%
% %% Display on the bottom right  (C scans)
%
% for i = 1:2
%     boundaryLeft = 0.5+(interval +Wscale)*(i-1);
%     boundaryBottom = 0.5+interval;
%     subplot('position',[boundaryLeft,1-Wscale-boundaryBottom,Wscale,Hscale]);
%     imagesc((squeeze(flipdim(B_scan(4+i,:,:),2))));axis off;
%     ylim([250 1014])
%     set(gca,'ydir','normal');
%     text(boundaryLeft*100,(0.5 + interval+Hscale)*290,['Bscan',num2str(frameN_B(i+4))],'Color','y','fontsize',10,'fontweight','bold');
% end
%
% for i = 3:4
%     boundaryLeft = 0.5+(interval +Wscale)*(i-2-1);
%     boundaryBottom = 0.5 + interval+Hscale;
%     subplot('position',[boundaryLeft,1-Wscale-boundaryBottom,Wscale,Hscale]);
%     imagesc(squeeze(slow_scan(i+2,:,:)));axis off;
%     ylim([10 800])
%     text(boundaryLeft*80,(0.5 + interval+Hscale)*1080,['Slow scan',num2str(frameN_slow(i+2))],'Color','y','fontsize',10,'fontweight','bold');
% end
%
% colormap(gray)

%%
set(gcf, 'Position', get(0, 'Screensize'));
% if contains(fn,'.unp')
%     fn = fn(1:end-4);
% end
% if fin ==1
% saveas(gcf,fullfile('/ensc/IMAGEBORG/PROJECTS/ONH_Longitudinal_ECC/Segment_Check',strcat(fn,'.png')));
% else
spath = strrep(fp, 'BVsmooth', 'Segment_check');
if ~exist(fullfile(spath), 'dir')
	mkdir(spath);
end

saveas(gcf, fullfile(spath, strcat(fn, '.tif')));   
% end

fig =1;
close all
end