function mat2tiff(fn)
    
    currFolder = pwd;
    %addpath(fullfile('/ensc/IMAGEBORG/DATA/ECC/mcorr', fn(10:13)));
    
    
    %load in volume
    volume = load(fn);
    if isstruct(volume)
        name = fieldnames(volume);
        name = name{1};
        eval(['volume = volume.', name,';']);
    end

    %Create new filename
    if contains(fn, '.mat')
        outputFileName = strrep(fn,'.mat','.tiff');
    else
        outputFileName = strcat(fn, '.tiff');
    end
    
    %Replace any non-real numbers
    volume(isinf(volume)) = 0;
    
   
    %Save as tiff stack
    for K=1:size(volume,3)
        imwrite(mat2gray(abs(volume(:,:,K))), fullfile('/ensc/IMAGEBORG/DATA/MouseSmaller/SuperAvg_OCT_S', outputFileName),'WriteMode','append','Compression','none');
    end

end