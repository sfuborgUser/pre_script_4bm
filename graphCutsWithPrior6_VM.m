  function heightMaps = graphCutsWithPrior(costImg, delX, limitsMaxMin, priorSurfaces, margins)
% Segments an image into multiple surfaces using graph cuts. Surfaces are
% calculated as height maps (one x height per y,z coordinate)
% costImg - LxMxN image of positive values. Higher values are more likely to be surface
% delX - scalar double smoothness constraint. change in height allowed between neighbouring surface points
% limitsMaxMin - (K-1)x2 matrix of intersurface distances. First column is
%       maximum distace between surfaces within each column, Second column is
%       minimum distance. K is the number of surfaces.
% priorSurfaces - Kx1 cell, with 1 element per surface that is either
%           1) 1xMxN arrays representing height maps, or
%           2) [], representing no constraint
% margins - scalar representing allowable deviation from the prior
assert(numel(delX)==1 & rem(delX,1)==0, 'delX must be a scalar integer');
assert(all(rem(limitsMaxMin(:),1)==0), 'limitsMaxMin must be all integers');
assert(all(cellfun(@(M) all(rem(M(:),1)==0),priorSurfaces)),'priorSurfaces must all be integer heights');
% assert(numel(margins(:))==1 & rem(margins(:),1)==0,'margins must be a scalar integer');
assert(all(rem(margins(:),1)==0), 'limitsMaxMin must be all integers');

tic
k=size(limitsMaxMin,1)+1;

weightImg=repmat([costImg(1,:,:);...
                  costImg(2:end,:,:)-costImg(1:end-1,:,:)],[1,1,1,k]);
nX = size(costImg,1);nY=size(costImg,2);nZ=size(costImg,3);
              
nNodes = numel(costImg)*k;
indicesSz = [size(costImg),k];
indices = reshape(1:nNodes,indicesSz);
deficientLow = false(indicesSz);
deficientHigh = false(indicesSz);
VB = false(indicesSz);

du = limitsMaxMin(:,1);
dl = limitsMaxMin(:,2);
eFrom={};
eTo={};

[priorGridY,priorGridZ] = ndgrid(1:nY,1:nZ);

mySub2ind = @(s,varargin) deal(1+sum((cell2mat(varargin)-1)*diag([1,cumprod(s(1:end-1))]),2),all(cell2mat(varargin)>=1 & bsxfun(@le,cell2mat(varargin),s),2));

for it = 2:k
    deficientLow(sum(dl(1:it-1)),:,:,it)=1;
end
for it = 1:k-1
    deficientHigh(indicesSz(1)+1-sum(dl(it:k-1)),:,:,it)=1;
end
for it = 1:k
if ~isempty(priorSurfaces{it})
    [defIndices,valid] = mySub2ind(indicesSz,priorSurfaces{it}(:)-margins(it)-1,priorGridY(:),priorGridZ(:),it*ones(nY*nZ,1));
    deficientLow(defIndices(valid))=1;
    [defIndices,valid] = mySub2ind(indicesSz,priorSurfaces{it}(:)+margins(it)+1,priorGridY(:),priorGridZ(:),it*ones(nY*nZ,1));
    deficientHigh(defIndices(valid))=1;
    for it2 = it+1:k
        [defIndices,valid] = mySub2ind(indicesSz,priorSurfaces{it}(:)-margins(it)-1+sum(dl(it:it2-1)),priorGridY(:),priorGridZ(:),it2*ones(nY*nZ,1));
        deficientLow(defIndices(valid))=1;
        [defIndices,valid] = mySub2ind(indicesSz,priorSurfaces{it}(:)+margins(it)+1+sum(du(it:it2-1)),priorGridY(:),priorGridZ(:),it2*ones(nY*nZ,1));
        deficientHigh(defIndices(valid))=1;
    end
    for it2 = it-1:-1:1
        [defIndices,valid] = mySub2ind(indicesSz,priorSurfaces{it}(:)-margins(it)-1-sum(du(it2:it-1)),priorGridY(:),priorGridZ(:),it2*ones(nY*nZ,1));
        deficientLow(defIndices(valid))=1;
        [defIndices,valid] = mySub2ind(indicesSz,priorSurfaces{it}(:)+margins(it)+1-sum(dl(it2:it-1)),priorGridY(:),priorGridZ(:),it2*ones(nY*nZ,1));
        deficientHigh(defIndices(valid))=1;
    end
end
end
deficientHigh = logical(cumsum(deficientHigh));
deficientLow = cumsum(deficientLow(end:-1:1,:,:,:));
deficientLow = logical(deficientLow(end:-1:1,:,:,:));

for it=1:k
    [vIndices,valid] = mySub2ind(indicesSz,1+reshape(sum(deficientLow(:,:,:,it),1),[],1),priorGridY(:),priorGridZ(:),it*ones(nY*nZ,1));
    VB(vIndices(valid)) = 1;
end

nondeficient = ~deficientHigh & ~deficientLow;
[gx,gy,gz,gk] = ndgrid(1:size(costImg,1),1:size(costImg,2),1:size(costImg,3),1:k);

% Intracolumn
eFrom{end+1} = indices(nondeficient & (gx>1));
eTo{end+1}   = indices(nondeficient & (gx>1))-1;

% Intercolumn Y
eFrom{end+1} = indices(nondeficient & (gy>1) & (gx>delX));
eTo{end+1}   = indices(nondeficient & (gy>1) & (gx>delX))-indicesSz(1)-delX;
eFrom{end+1} = indices(nondeficient & (gy<indicesSz(2)) & (gx>delX));
eTo{end+1}   = indices(nondeficient & (gy<indicesSz(2)) & (gx>delX))+indicesSz(1)-delX;
%Intercolumn Z
eFrom{end+1} = indices(nondeficient & (gz>1) & (gx>delX));
eTo{end+1}   = indices(nondeficient & (gz>1) & (gx>delX))-prod(indicesSz(1:2))-delX;
eFrom{end+1} = indices(nondeficient & (gz<indicesSz(3)) & (gx>delX));
eTo{end+1}   = indices(nondeficient & (gz<indicesSz(3)) & (gx>delX))+prod(indicesSz(1:2))-delX;


%Intersurface
for it = 1:k-1
    eFrom{end+1} = indices(nondeficient & gk==it & gx<indicesSz(1)-dl(it));
    eTo{end+1}   = indices(nondeficient & gk==it & gx<indicesSz(1)-dl(it))+dl(it)+prod(indicesSz(1:3));

    eFrom{end+1} = indices(nondeficient & gk==it+1 & gx>du(it));
    eTo{end+1}   = indices(nondeficient & gk==it+1 & gx>du(it))-du(it)-prod(indicesSz(1:3));
end

% connect Vbase
eFrom{end+1} = find(VB);
eTo{end+1} = eFrom{end}([2:end,1]);

% combine edges into a pair of vectors
[eFrom2,eTo2] = cellfun(@(eF,eT) deal(eF(:),eT(:)),eFrom,eTo,'uniformoutput',false);
[eFrom2,eTo2] = deal(cell2mat(eFrom2')',cell2mat(eTo2')');

% remove edges that connect to a deficient node
deficientEdges = deficientLow(eFrom2)|deficientHigh(eFrom2)|deficientLow(eTo2)|deficientHigh(eTo2);
eFrom2 = eFrom2(~deficientEdges)';
eTo2 = eTo2(~deficientEdges)';

% Format for mex graph cuts algorithm
if 1
% find duplicated edges
% equivalent to duplicated = ismember([eFrom2,eTo2],[eTo2,eFrom2],'rows');
%  but 4 times faster
% tic
% M=sparse(eFrom2,eTo2,1:length(eFrom2),nNodes,nNodes);
% MM = M&M';
% duplicated = false(length(eFrom2),1);
% duplicated(M(MM))=true;
% 
% deduplicated = unique(sort([eFrom2(duplicated),eTo2(duplicated)],2),'rows');
% eFrom2 = [eFrom2(~duplicated);deduplicated(:,1)];
% eTo2 = [eTo2(~duplicated);deduplicated(:,2)];
% weights = [repmat([1e20,0],nnz(~duplicated),1);inf(size(deduplicated,1),2)];
% toc
weights = repmat([1e20,0],length(eFrom2),1);


weightImg(VB) = -1;

deficient = deficientHigh | deficientLow;

mask=weightImg(~deficient)<0;
awI = abs(weightImg(~deficient))+1;
sourceWeights = awI(:).*mask(:) - 1;
sinkWeights = awI(:).*~mask(:) - 1; 

map1 = zeros(numel(deficient),1);
map1(~deficient)=1:nnz(~deficient);

eFrom2 = map1(eFrom2)';
eTo2 = map1(eTo2)';
nNodes = nnz(~deficient);

 if ~(exist('mxMaxFlow2')==3)
     addpath('\\bluebell\ensc\IMAGE_MAIN\SOFTWARE\GraphCuts')
 end
 output=mxMaxFlow2(nNodes,int32(eFrom2),int32(eTo2),weights(:,1),weights(:,2),sourceWeights,sinkWeights);
 map2 = find(nondeficient);
 heightMaps = accumarray([gy(map2(output)),gz(map2(output)),gk(map2(output))],gx(map2(output)),indicesSz(2:4),@max);
 heightMaps = cellfun(@(h)permute(h,[3,1,2]),num2cell(heightMaps,[1,2]),'uniformoutput',false);
end

% if 0
% edges=sparse(eFrom2,eTo2,inf,nNodes,nNodes);
% 
% grid = [gx(:),gy(:)+(size(costImg,2)+2)*(gk(:)-1),gz(:)];
% % grid(end+1,:) = [size(costImg,1)+1,size(costImg,2)/2,size(costImg,3)/2];
% % grid(end+1,:) = [0,size(costImg,2)/2,size(costImg,3)/2];
% clf;hold on
% scatter3(grid(deficientLow,1),grid(deficientLow,2),grid(deficientLow,3),5,[1,0,0]);
% scatter3(grid(deficientHigh,1),grid(deficientHigh,2),grid(deficientHigh,3),5,[0,0,1]);
% scatter3(grid(~(deficientLow|deficientHigh),1),grid(~(deficientLow|deficientHigh),2),grid(~(deficientLow|deficientHigh),3),10,[0,1,0]);
% [eFromD,eToD] = find(edges);
% quiver3(grid(eFromD,1),grid(eFromD,2),grid((eFromD),3),...
%         grid(eToD,1)-grid((eFromD),1),grid((eToD),2)-grid((eFromD),2),grid((eToD),3)-grid((eFromD),3),0);
% 
% view(-90,88);
% end

