function AD_gc_script_1BM(loadloc,filename,savepath,Nsteps,volNo)


if ~exist(savepath)
    mkdir(savepath)
end

load(fullfile(loadloc,[filename,'_',num2str(volNo),'-BV_it',num2str(Nsteps),'.mat']));
V2 = (2*32768*mat2gray(V))-32768;
V2 = double(V2);

%Load Acquisition Parameters
fn2 = strcat(filename,'.xml');
parameters  = getParameters(fn2);
numPoints   = parameters(1);


% V5 = flip(V2(:, :, 1:end),1);
V5 = V2;

% ILM
slope = 1; noop=@(M) M; negate = @(M) -M;
surfDist1 = [270,10]; %was 270/50
surflines = [4,1];  numSurf = size(surflines,1);
instructions = {{[],[]},            [0],                 8, 4,          surfDist1,       5, noop;...   % ILM
    {[1,2]},            [0],                 4, 2,          zeros(0,2),      5, noop;...
    {[2,1]},            [0],                 2, 1,          zeros(0,2),      5, noop;...
    {[3,1]},            [0],                 1, 1,          zeros(0,2),      5, noop;...  % IO based on ILM
    };
gc = graphCut3D_prior(V5, numSurf, slope, instructions, surflines,0);
gcILM = squeeze(gc{1});
h = fspecial('disk',10);
gcILM = round(imfilter(gcILM,h,'replicate'));
save(fullfile(savepath,[filename,'_',num2str(volNo),'-ILMseg.mat']),'gcILM','-v7.3');

% % % ONL
% % % tic
% slope = 1; noop=@(M) M; negate = @(M) -M;
%     surfDist1 = [200,50]; %was 150/10
%     surflines = [4,1]; numSurf = size(surflines,1);
%     instructions = {{[],[]},        [0],                   8, 4,            surfDist1,        2, negate;...
%         {[1,1]},        [0],                   4, 2,            zeros(0,2),       2, negate;...
%         {[2,1]},        [0],                   2, 1,            zeros(0,2),       2, negate;...
%         {[3,1]},        [0],                   1, 1,            zeros(0,2),       2, negate;...
%         };
% gc = graphCut3D_prior(V5, numSurf, slope, instructions, surflines,0);
% gcELM = squeeze(gc{1});
% % filter
% h = fspecial('disk', 30);
% gcONL = round(imfilter(gcELM,h,'replicate'));
% save(fullfile(savepath,[filename,'_',num2str(volNo),'-ONLseg.mat']),'gcONL','-v7.3');
toc


end
%end



