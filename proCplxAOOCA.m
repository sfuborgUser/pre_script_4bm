function matvol = proCplxAOOCA(fn,svsave,loadloc)

%Load motion corrected volume and divide into the three frames
filename = strrep(fn,'.unp','    _mcorr.mat');
load(fullfile(loadloc,filename));
%Load Acquisition Parameters
parameters  = getParameters(strcat(fn(1:end-6),'.xml'));

numBMscans = parameters(5);
numFrames = parameters(3);
strtFrame = 1;
lastFrame = (numFrames/numBMscans)*numBMscans + (strtFrame-1);
cplxVol = (volume_mcorr(:,:,strtFrame:lastFrame));
clear volume_mcorr

%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Main OCTA Process %%%
%%%%%%%%%%%%%%%%%%%%%%%%%


% Average OCT %
avgCplxVol = zeros([size(cplxVol,1) size(cplxVol,2) ((size(cplxVol,3)/numBMscans))]);
for I = 1:numBMscans:size(cplxVol,3)
    K = ((I-1)/numBMscans)+1;
    for J = 1:numBMscans
        cplxConjX     = cplxVol(:,:,I+(J-1)).*conj(cplxVol(:,:,I));
        bulkOffset = angle(sum(cplxConjX,1));
        avgCplxVol(:,:,K) = avgCplxVol(:,:,K) + (cplxVol(:,:,I+(J-1))...
            .*exp(-1j*repmat(bulkOffset, [size(cplxConjX,1) 1])));
    end
end
avgOctVol_dB = 20.*log10(abs(avgCplxVol./(numBMscans)));

%%%% OCT-A process %%%%

for I = 1:numBMscans:size(cplxVol,3)
    K = ((I-1)/numBMscans)+1;
    
    %%%% Speckle Variance & Complex Variance %%%%
    if numBMscans == 3
        Xconj_1 = cplxVol(:,:,I+1).*conj(cplxVol(:,:,I));
        Xconj_2 = cplxVol(:,:,I+2).*conj(cplxVol(:,:,I));
        
        BulkOff_1 = repmat(angle(sum(Xconj_1,1)), [size(Xconj_1,1) 1]);
        BulkOff_2 = repmat(angle(sum(Xconj_2,1)), [size(Xconj_2,1) 1]);
        
        sv = var(cat(3,abs(cplxVol(:,:,I)),abs(cplxVol(:,:,I+1)),abs(cplxVol(:,:,I+2))),0,3);
        cv = var(cat(3,(cplxVol(:,:,I)),(cplxVol(:,:,I+1).*exp(-1j*BulkOff_1)),(cplxVol(:,:,I+2).*exp(-1j*BulkOff_2))),0,3);
    else
        Xconj_1 = cplxVol(:,:,I+1).*conj(cplxVol(:,:,I));
        Xconj_2 = cplxVol(:,:,I+2).*conj(cplxVol(:,:,I));
        Xconj_3 = cplxVol(:,:,I+3).*conj(cplxVol(:,:,I));
        
        BulkOff_1 = repmat(angle(sum(Xconj_1,1)), [size(Xconj_1,1) 1]);
        BulkOff_2 = repmat(angle(sum(Xconj_2,1)), [size(Xconj_2,1) 1]);
        BulkOff_3 = repmat(angle(sum(Xconj_3,1)), [size(Xconj_3,1) 1]);
        
        sv = var(cat(3,abs(cplxVol(:,:,I)),abs(cplxVol(:,:,I+1)),abs(cplxVol(:,:,I+2)),abs(cplxVol(:,:,I+3))),0,3);
        cv = var(cat(3,(cplxVol(:,:,I)),(cplxVol(:,:,I+1).*exp(-1j*BulkOff_1)),(cplxVol(:,:,I+2).*exp(-1j*BulkOff_2)),(cplxVol(:,:,I+3).*exp(-1j*BulkOff_3))),0,3);
    end
    
%         SV(:,:,K) = imadjust((sv-min(sv(:)))./(max(sv(:))-min(sv(:))));
%         CV(:,:,K) = imadjust((cv-min(cv(:)))./(max(cv(:))-min(cv(:))));
   SV(:,:,K) = sv;
   CV(:,:,K) = cv;
    
    %%%% Differential Averaging & Variance %%%%
    Xconj_A   = cplxVol(:,:,I+1).*conj(cplxVol(:,:,I));
    Xconj_B   = cplxVol(:,:,I+2).*conj(cplxVol(:,:,I+1));
    Xconj_C   = cplxVol(:,:,I).*conj(cplxVol(:,:,I+2));
    BulkOff_A  = repmat(angle(sum(Xconj_A,1)), [size(Xconj_A,1) 1]);
    BulkOff_B  = repmat(angle(sum(Xconj_B,1)), [size(Xconj_B,1) 1]);
    BulkOff_C  = repmat(angle(sum(Xconj_C,1)), [size(Xconj_C,1) 1]);
    
    Dcplx_A = (cplxVol(:,:,I+1).*exp(-1j*BulkOff_A) - cplxVol(:,:,I));
    Dcplx_B = (cplxVol(:,:,I+2).*exp(-1j*BulkOff_B) - cplxVol(:,:,I+1));
    Dcplx_C = (cplxVol(:,:,I).*exp(-1j*BulkOff_C) - cplxVol(:,:,I+2));
    
    DA(:,:,K) = mean(cat(3,abs(Dcplx_A),abs(Dcplx_B),abs(Dcplx_C)),3);
    DV(:,:,K) = var(cat(3,abs(Dcplx_A),abs(Dcplx_B),abs(Dcplx_C)),0,3);
end

avgSV = mov2Davg(SV, [3, 3]);
%avgCV = mov2Davg(CV, [3, 3]);
avgDA = mov2Davg(DA, [3, 3]);
avgDV = mov2Davg(DV, [3, 3]);

avgOctVol_dB = avgOctVol_dB(1:end,:,:);
avgDAV      = avgDA(1:end,:,:);
avgDPV      = avgDV(1:end,:,:);
avgDCV      = avgCV(1:end,:,:);
avgSV       = avgSV(1:end,:,:);


if ~exist(svsave,'dir')
    mkdir(svsave);
end

% save(fullfile(intsave,[fn(1:end-4),'_intensity.mat']), 'avgOctVol_dB', '-v7.3'); % dynamic phase variance
save(fullfile(svsave, [fn(1:end-4),'_DAV.mat']),       'avgDAV',       '-v7.3'); % dynamic phase variance
% save(fullfile(svsave, [fn(1:end-4),'_DPV.mat']),       'avgDPV',       '-v7.3'); % dynamic amplitude variance
% save(fullfile(svsave, [fn(1:end-4),'_DCV.mat']),       'avgDCV',       '-v7.3'); % dynamic amplitude variance
% save(fullfile(svsave, [fn(1:end-4),'_SV.mat']),        'avgSV',        '-v7.3'); % speckle variance
end


