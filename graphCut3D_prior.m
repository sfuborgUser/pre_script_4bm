function [hmap] = graphCut3D_prior(img, numSurf, slope, instructions, surfs,priors)

img2 = flipdim(img,1); clear img;
%img2 = img;

% 1. Define functions
drawPriorSurface = @(M,sz) cumsum(accumarray([M(:),reshape(repmat((1:size(M,2))',size(M,3),1),[],1),reshape(repmat(1:size(M,3),size(M,2),1),[],1)],1,[sz,size(M,2),size(M,3)]));
unsubsample = @(M,n) reshape(repmat(permute(M,[1,2,4,3]),[n,1,n,1]),[1,size(M,2)*n,size(M,3)*n]); 
noop=@(M) M;

negate = @(M) -M;
itNum = size(instructions,1);       

% 2. Segmentation - loop through each line of instructions
for it = 1:itNum
    %it    
    % a. re-set parameters. scale grayscale and recompute cost image.
    inst = instructions(it,:);
    sX=inst{3}; sYZ=inst{4};     
    viewImg{it} = img2(1:sX:end,1:sYZ:end,1:sYZ:end); 
    costImg = [viewImg{it}(2,:,:)-viewImg{it}(1,:,:); (viewImg{it}(3:end,:,:)-viewImg{it}(1:end-2,:,:))/2; viewImg{it}(end,:,:)-viewImg{it}(end-1,:,:)];

    % b. scale priors (it = segmentation iteration, it2 = priors) % what is number of priors? number of surfaces? 
    for it2 = 1:length(inst{1}) 
        if isempty(inst{1}{it2})    % no 
            priorSurfaces{it}{it2} = [];
        elseif any(inst{1}{it2}(1)==100)
            % given prior is 1 x 400 x 400 (or 1 x full ydim of the original volume x full zdim of the original volume, with x values also scaled to a full volume)
            % need to get this down to 1 x sYZ x sYZ with x value scaled to
            % sX
            myPrior = cell2mat(priors(inst{1}{it2}(2)));
            myPrior = size(img2,1)-myPrior;   % flip priors
            myPrior = myPrior(:,1:sYZ:end,1:sYZ:end);
            myPrior = round(myPrior./sX);
            priorSurfaces{it}{it2} = round(myPrior+inst{2}(it2)/sX);
        else
            % downsample scaling ratio between prior and current 
            xUpsample = instructions{inst{1}{it2}(1),3}/sX; % prior's downsample rate / current downsample rate 
            yzUpsample = instructions{inst{1}{it2}(1),4}/sYZ;           
            % match yz scale  
            if yzUpsample > 1
                priorSurfaces{it}{it2} = unsubsample(heightMaps{inst{1}{it2}(1)}{inst{1}{it2}(2)},yzUpsample);
            else
                priorSurfaces{it}{it2} = heightMaps{inst{1}{it2}(1)}{inst{1}{it2}(2)};
            end            
            % match x scale
            priorSurfaces{it}{it2} = round(max(1,inst{2}(it2)/sX + 1+xUpsample*(priorSurfaces{it}{it2}(:,1:size(costImg,2),1:size(costImg,3))-1)));
        end
    end
    
    % c. graphcut
%     tic
    heightMaps{it} = graphCutsWithPrior6_VM(inst{7}(costImg), ceil(slope/sX*sYZ), ceil(inst{5}/sX), priorSurfaces{it}, ceil(inst{6}/sX));
%     toc 
end

% 3. Return 
myImg = flipdim(viewImg{surfs(1)},1);
for sCount = 1:size(surfs,1)
    hmap{sCount} = size(myImg,1) - heightMaps{surfs(sCount,1)}{surfs(sCount,2)};
end

% % 7. Visualization 
% myImg = flipdim(viewImg{3},1);
% hmap1 = size(myImg,1)-heightMaps{3}{1};
% hmap2 = size(myImg,1)-heightMaps{6}{1};
% hmap3 = size(myImg,1)-heightMaps{6}{2};
% 
% for myi=1:size(myImg,3)
%     myi
%     imagesc(myImg(:,:,myi)); colormap gray
%     hold on;
%     plot(1:size(myImg,2),hmap2(1,:,myi),'-m','LineWidth',0.7)
% %      plot(1:size(myImg,2),hmap2(1,:,myi),'-r','LineWidth',0.7)
% %      plot(1:size(myImg,2),hmap3(1,:,myi),'-g','LineWidth',0.7)
%     pause;
% end
% 
% figure;
% for myi = 0:8
%     subplot(3,3,myi+1);
%     frind = myi*25; 
%     if frind==0
%         frind = frind+1;
%     end
%         
%     imagesc(myImg(:,:,frind)); colormap gray
%     hold on;
%      plot(1:size(myImg,2),hmap1keep(1,:,frind),'LineWidth',0.7)
%      plot(1:size(myImg,2),hmap2keep(1,:,frind),'-r','LineWidth',0.7)
%      plot(1:size(myImg,2),hmap3keep(1,:,frind),'-g','LineWidth',0.7)
%     hold off;
% end
%     
%   
