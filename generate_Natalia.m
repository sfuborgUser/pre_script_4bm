
clear all;

%Input all subjects you wish to batch process
dataL = input('What are the subjects you wish to process? Please enter using string matrix format. ');

% Finds all folders associated with each subject number
for count = 1:length(dataL)
    subject = char(dataL(count));
    cd(fullfile('/ensc/IMAGEBORG/DATA/ECC/RAW', subject));
    
    
    list = dir;
    L = length(list);
    counter = 0;
    
    % Due to our lovely file structure the next set of folders should be
    % date folders. This loop places any folders or files in this directory into a
    % temporary variable.
    for i = 3:L
        tempDate = string(list((i),1).name);
        
        % Checks if the given string is a folder and follows the format
        % yyyy.mm.dd. If yes the folder is placed into a string vector
        if (regexp(tempDate, '^[0-9]+\.[0-9]+\.[0-9]+') == 1) & isfolder(tempDate)
            counter = counter + 1;
            dateFolders(counter) = tempDate;
        end
    end
    
    % Inputting in the rest of the files to prep to cd into the 200 khz folder
    scanType = 'OCTA';
    f = '200khz';
    
    % If there is only one date folder
    if length(dateFolders) == 1
        date = char(dateFolders(1));
        cd(fullfile('/ensc/IMAGEBORG/DATA/ECC/RAW', subject, date));
        folders = dir;
        
    % If there is more than one date folder the options are displayed to
    % the screen and the user chooses which folder to cd into for that
    % patient
    else
        fprintf('For subject %.4s there are %d date folders.\n', subject, length(dateFolders))
        disp(' ')
        
        %Displays options
        for j = 1:length(dateFolders)
            fprintf('%d) %s\n', j, dateFolders(j))
        end
        
        disp(' ')
        num = input('Which folder would you like to cd into? ');
        
        %cd into the chosen file
        date = char(dateFolders(num));
       % cd(fullfile('/ensc/IMAGEBORG/DATA/ECC/RAW', subject, date, scanType, f));
                cd(fullfile('/ensc/IMAGEBORG/DATA/ECC/RAW', subject, date));

        folders = dir;
    end
        


for i = 3:size(folders,1)

	cd(folders(i).name);
	xml = dir('*xml');

	for j = 1:size(xml,1)
		if ~exist('files')
			files(1,1) = cellstr(xml(j).name);
		else
			files(end+1,1) = cellstr(xml(j).name);
		end
	end

	cd('..');

end

    % for i = 2:size(files,1)
    %     fileName = strtrim(char(files{i}));
    %     fileName = fileName(:,1:end-4);
    %     cd('/ensc/IMAGEBORG/DATA/ECC/scripts/final_AO/final/')
    %     AO_pre_script_2BM(fileName,0,1)
    % end
% addpath('/ensc/IMAGEBORG/DATA/ECC/RAW/H346');    
% 
% cd(fullfile('/ensc/IMAGEBORG/DATA/ECC/RAW/H346/2018.06.14'));    
%     
% Folder = pwd;
% FileList = dir(fullfile(Folder, '**', '*.xml'));
% 
% for ii = 1:size(FileList, 1)
%     
%     if strcmp(strcat(filename, '.xml'), FileList(ii).name)
%         directory = FileList(ii).folder;
%     end
%     
% end
    

    %addpath(genpath('/ensc/IMAGEBORG/DATA/ECC/scripts/final_AO/final'))

    commandString1 = '#PBS -W group_list=ensc-imageborg';
    commandString2 = '#PBS -l nodes=1:ppn=1';
    commandString3 = '#PBS -l walltime=23:00:00';
    commandString4 = '#PBS -l pmem=26gb';
    commandString5 = '#PBS -m bea';
    commandString6 = 'cd /ensc/IMAGEBORG/STUDENTS/FRANCIS/GitRepo/Pre_script_4BM';

    %PR = 1;
    %OCTA = 1;

    fclose('all');
    for i = 1:size(files,1)
        fileName = strtrim(char(files{i}));
        fileName = fileName(:,1:end-4);
% %         fileName = FileList(i).name;
% %         fileName = fileName(1:end-4)
        fopen(fileName,'w+');
        fclose('all');
        fileNameFull = fullfile('/ensc/IMAGEBORG/DATA/ECC/scripts/final/OCT/',fileName);
        fileID = fopen(fileNameFull,'w');
        commandString = ['matlab -nodisplay -nodesktop -r "Pre_script(''',fileName,'''); exit"'];
        fprintf(fileID,'%s\n',commandString1);
        fprintf(fileID,'%s\n',commandString2);
        fprintf(fileID,'%s\n',commandString3);
        fprintf(fileID,'%s\n',commandString4);
        fprintf(fileID,'%s\n',commandString5);
        fprintf(fileID,'%s\n',commandString6);
        fprintf(fileID,'%s\n',commandString);
        fclose(fileID);   
%         delete(fileName);
    end

    allJobsFileName = '/ensc/IMAGEBORG/DATA/ECC/scripts/final/OCT/ALL_JOBS_SUBMIT_LIST_AO.sh';
    %cd('/ensc/IMAGEBORG/DATA/ECC/scripts/final_AO/MJ');
    fileID_ALL_JOBS = fopen(allJobsFileName, 'w+');
    for i = 1:size(files,1)
        fileName = strtrim(char(files{i}));
        fileName = fileName(:,1:end-4);
        fprintf(fileID_ALL_JOBS,'%s\n',['qsub ',fileName]);
% 		fprintf(fileID_ALL_JOBS,'%s\n',['Pre_script(''',fileName, ''');']);

    end
    fclose(fileID_ALL_JOBS);
    
end


clear all;

