
%Load in the 2 volumes you want to register

warning off;
% V1 = load('/ensc/IMAGEBORG/DATA/ECC/mcorr/H777/2018.09.28/H777-OS-area1/13_41_16-H777-OS-area1_0    _mcorr.mat');
% V2 = load('/ensc/IMAGEBORG/DATA/ECC/mcorr/H777/2018.09.28/H777-OS-area1/13_41_27-H777-OS-area1_0    _mcorr.mat');

V1 = load(fullfile('/ensc/IMAGEBORG/DATA/ECC/mcorr/H777/2018.09.28/H777-OS-area1/13_41_16-H777-OS-area1_0    _mcorr.mat'));
V2 = load(fullfile('/ensc/IMAGEBORG/DATA/ECC/mcorr/H777/2018.09.28/H777-OS-area1/13_41_16-H777-OS-area1_0    _mcorr.mat'));

vol2 = abs(V2.volume_mcorr);
vol1 = abs(V1.volume_mcorr);

clear V1; clear V2; 

% Affine Registration
[optimizer, metric] = imregconfig('monomodal');
tform = imregtform(vol2,vol1,'affine', optimizer, metric);
movingRegistered = imwarp(vol2,tform,'OutputView',imref3d(size(vol1)));
figure; 
for i = 1:size(vol1,3)
	imshowpair(vol1(:,:,i),movingRegistered(:,:,i),'montage')
	pause(0.1)
end

% Non-Rigid Registration
[D,moving_reg] = imregdemons(movingRegistered,vol1);

V3 = moving_reg;

for i = 100:size(vol1,1)-10
	imshowpair(imadjust(mat2gray(squeeze(mean(vol1(i:i+10,:,:),1)))),imadjust(mat2gray(squeeze(mean(V3(i:i+10,:,:),1)))),'montage');
	pause(0.1)
end
