function TSASSIE_GEN(loadloc)

tic
close all

filenames = dir(fullfile(loadloc,['*all*','.tif']));

% The OCTA images
im1_all = imread(fullfile(loadloc,filenames(1).name));
im1_all = zeros(size(im1_all,1)-25, size(im1_all,2)-25); %%%%% The -25 is to get rid of noise present in some OCTA images
im1_opl = zeros(size(im1_all,1), size(im1_all,2));
im1_sup = zeros(size(im1_all,1), size(im1_all,2));
im1_int = zeros(size(im1_all,1), size(im1_all,2));

% The intensity images
% im1_in_all = zeros(size(im1_all,1), size(im1_all,2));
% im1_in_opl = zeros(size(im1_all,1), size(im1_all,2));
% im1_in_sup = zeros(size(im1_all,1), size(im1_all,2));
% im1_in_int = zeros(size(im1_all,1), size(im1_all,2));

thresh = 30;
sizestrips = 125; % IMPORTANT: This should be set to a size that is evenly divisIble by the cropped image size

%Create Strips
for i = 1:size(filenames,1)
    filename = filenames(i).name;
    %[imout_all, imout_opl, imout_sup, imout_int, imout_in_all, imout_in_opl, imout_in_sup, imout_in_int] = sv_strip_templateless_GEN(loadloc,filename, thresh, sizestrips);
    [imout_all, imout_opl, imout_sup, imout_int] = sv_strip_templateless_GEN(loadloc,filename, thresh, sizestrips);

    for ii = 1:size(imout_all,3)
        figure, imshowpair(imout_all(:,:,ii), imout_opl(:,:,ii), 'montage')
    end
    
    close all
    
    im1_all(:,:,size(im1_all,3)+1:size(im1_all,3)+size(imout_all,3)) = imout_all;
    im1_opl(:,:,size(im1_opl,3)+1:size(im1_opl,3)+size(imout_all,3)) = imout_opl;
    im1_sup(:,:,size(im1_sup,3)+1:size(im1_sup,3)+size(imout_all,3)) = imout_sup;
    im1_int(:,:,size(im1_int,3)+1:size(im1_int,3)+size(imout_all,3)) = imout_int;
    
%     im1_in_all(:,:,size(im1_in_all,3)+1:size(im1_in_all,3)+size(imout_in_all,3)) = imout_in_all;
%     im1_in_opl(:,:,size(im1_in_opl,3)+1:size(im1_in_opl,3)+size(imout_in_all,3)) = imout_in_opl;
%     im1_in_sup(:,:,size(im1_in_sup,3)+1:size(im1_in_sup,3)+size(imout_in_all,3)) = imout_in_sup;
%     im1_in_int(:,:,size(im1_in_int,3)+1:size(im1_in_int,3)+size(imout_in_all,3)) = imout_in_int;
    
    clear imout

end

% Delete all blank frames
im1_all(:,:,all(all(im1_all==0,1),2)) = [];
im1_opl(:,:,all(all(im1_opl==0,1),2)) = [];
im1_sup(:,:,all(all(im1_sup==0,1),2)) = [];
im1_int(:,:,all(all(im1_int==0,1),2)) = [];

% im1_in_all(:,:,all(all(im1_in_all==0,1),2)) = [];
% im1_in_opl(:,:,all(all(im1_in_opl==0,1),2)) = [];
% im1_in_sup(:,:,all(all(im1_in_sup==0,1),2)) = [];
% im1_in_int(:,:,all(all(im1_in_int==0,1),2)) = [];

% Sort the strips by size to prepare for registration
for ii = 1:size(im1_all,3)
    stats = regionprops(logical(im1_all(:,:,ii)), 'BoundingBox');
    BB = stats.BoundingBox;
    Areas(ii) = BB(3)*BB(4);
end

sortedAreas = sort(Areas, 'descend');

% Create new strip arrays that contain the strips in descending order
% according to their size
ii = 1;
while (ii <= size(im1_all,3))
    
    index = find(Areas == sortedAreas(ii));
    
    if size(index,2) > 1
        for jj = 1:size(index,2)
            im2_all(:,:,ii) = padarray(im1_all(:,:,index(jj)), [25 25]);
            im2_opl(:,:,ii) = padarray(im1_opl(:,:,index(jj)), [25 25]);
            im2_sup(:,:,ii) = padarray(im1_sup(:,:,index(jj)), [25 25]);
            im2_int(:,:,ii) = padarray(im1_int(:,:,index(jj)), [25 25]);
            
%             im2_in_all(:,:,ii) = padarray(im1_in_all(:,:,index(jj)), [25 25]);
%             im2_in_opl(:,:,ii) = padarray(im1_in_opl(:,:,index(jj)), [25 25]);
%             im2_in_sup(:,:,ii) = padarray(im1_in_sup(:,:,index(jj)), [25 25]);
%             im2_in_int(:,:,ii) = padarray(im1_in_int(:,:,index(jj)), [25 25]);
                        
            ii = ii + 1;
        end
        continue
    end
    
    im2_all(:,:,ii) = padarray(im1_all(:,:,index), [25 25]);
    im2_opl(:,:,ii) = padarray(im1_opl(:,:,index), [25 25]);
    im2_sup(:,:,ii) = padarray(im1_sup(:,:,index), [25 25]);
    im2_int(:,:,ii) = padarray(im1_int(:,:,index), [25 25]);
    
%     im2_in_all(:,:,ii) = padarray(im1_in_all(:,:,index), [25 25]);
%     im2_in_opl(:,:,ii) = padarray(im1_in_opl(:,:,index), [25 25]);
%     im2_in_sup(:,:,ii) = padarray(im1_in_sup(:,:,index), [25 25]);
%     im2_in_int(:,:,ii) = padarray(im1_in_int(:,:,index), [25 25]);
    
    ii = ii + 1;
end

% Find any very large strips and break them up into smaller strips to
% improve SIFT registration
for ii = 2:size(im2_all, 3)
    
    stats = regionprops(logical(im2_all(:,:,ii)), 'BoundingBox');
    BB = stats.BoundingBox;
    if BB(3) > 200
        ind = 2;
        %         while ind == 1
        %             ind = randi(4);
        %         end
        
        % The point at which we will break up the large strip into 2
        % strips
        div1 = floor(BB(3)/ind + BB(1));
        %       div2 = 2*div1;
        
        temp_all = im2_all(:,:,ii);
        temp_opl = im2_opl(:,:,ii);
        temp_sup = im2_sup(:,:,ii);
        temp_int = im2_int(:,:,ii);
%         temp_in_all = im2_in_all(:,:,ii);
%         temp_in_opl = im2_in_opl(:,:,ii);
%         temp_in_sup = im2_in_sup(:,:,ii);
%         temp_in_int = im2_in_int(:,:,ii);
        
        im2_all(:,:,ii) = zeros(size(im2_all,1), size(im2_all,2));
        im2_opl(:,:,ii) = zeros(size(im2_all,1), size(im2_all,2));
        im2_sup(:,:,ii) = zeros(size(im2_all,1), size(im2_all,2));
        im2_int(:,:,ii) = zeros(size(im2_all,1), size(im2_all,2));
%         im2_in_all(:,:,ii) = zeros(size(im2_all,1), size(im2_all,2));
%         im2_in_opl(:,:,ii) = zeros(size(im2_all,1), size(im2_all,2));
%         im2_in_sup(:,:,ii) = zeros(size(im2_all,1), size(im2_all,2));
%         im2_in_int(:,:,ii) = zeros(size(im2_all,1), size(im2_all,2));
        
        
        im2_all(:,1:div1,ii) = temp_all(:,1:div1);
        im2_opl(:,1:div1,ii) = temp_opl(:,1:div1);
        im2_sup(:,1:div1,ii) = temp_sup(:,1:div1);
        im2_int(:,1:div1,ii) = temp_int(:,1:div1);
%         im2_in_all(:,1:div1,ii) = temp_in_all(:,1:div1);
%         im2_in_opl(:,1:div1,ii) = temp_in_opl(:,1:div1);
%         im2_in_sup(:,1:div1,ii) = temp_in_sup(:,1:div1);
%         im2_in_int(:,1:div1,ii) = temp_in_int(:,1:div1);
        
        
        im2_all(:,:,size(im2_all,3)+1) = zeros(size(im2_all,1), size(im2_all,2));
        im2_opl(:,:,size(im2_opl,3)+1) = zeros(size(im2_all,1), size(im2_all,2));
        im2_sup(:,:,size(im2_sup,3)+1) = zeros(size(im2_all,1), size(im2_all,2));
        im2_int(:,:,size(im2_int,3)+1) = zeros(size(im2_all,1), size(im2_all,2));
%         im2_in_all(:,:,size(im2_in_all,3)+1) = zeros(size(im2_all,1), size(im2_all,2));
%         im2_in_opl(:,:,size(im2_in_opl,3)+1) = zeros(size(im2_all,1), size(im2_all,2));
%         im2_in_sup(:,:,size(im2_in_sup,3)+1) = zeros(size(im2_all,1), size(im2_all,2));
%         im2_in_int(:,:,size(im2_in_int,3)+1) = zeros(size(im2_all,1), size(im2_all,2));
        
        
        im2_all(:, (div1+1):end, size(im2_all,3)) = temp_all(:,(div1+1):end);
        im2_opl(:, (div1+1):end, size(im2_all,3)) = temp_opl(:,(div1+1):end);
        im2_sup(:, (div1+1):end, size(im2_all,3)) = temp_sup(:,(div1+1):end);
        im2_int(:, (div1+1):end, size(im2_all,3)) = temp_int(:,(div1+1):end);
%         im2_in_all(:, (div1+1):end, size(im2_all,3)) = temp_in_all(:,(div1+1):end);
%         im2_in_opl(:, (div1+1):end, size(im2_all,3)) = temp_in_opl(:,(div1+1):end);
%         im2_in_sup(:, (div1+1):end, size(im2_all,3)) = temp_in_sup(:,(div1+1):end);
%         im2_in_int(:, (div1+1):end, size(im2_all,3)) = temp_in_int(:,(div1+1):end);
        
    end
end

%% Save strips
save(fullfile(loadloc, 'strips.mat'))

%% Uncomment and start from here if loading set of unregistered strips
%load('strips.mat')

%% Set the fixed strip and other initializations 

% Make the fixed strip the first strip in im2. This is a good idea since
% im2 is organized with respect to size, so making the fixed strip the
% largest strip is a safe bet. BUT NOT ALWAYS THE CASE. FML.
% Could develop a new metric for determining the fixed strip. Combination
% of size and quality (sharpness?)
im2_all(:,:,1) = mat2gray(im2_all(:,:,1));
im2_opl(:,:,1) = mat2gray(im2_opl(:,:,1));
im2_sup(:,:,1) = mat2gray(im2_sup(:,:,1));
im2_int(:,:,1) = mat2gray(im2_int(:,:,1));
% im2_in_all(:,:,1) = mat2gray(im2_in_all(:,:,1));
% im2_in_opl(:,:,1) = mat2gray(im2_in_opl(:,:,1));
% im2_in_sup(:,:,1) = mat2gray(im2_in_sup(:,:,1));
% im2_in_int(:,:,1) = mat2gray(im2_in_int(:,:,1));

fixed_all = im2_all(:,:,1);
fixed_opl = im2_opl(:,:,1);
fixed_sup = im2_sup(:,:,1);
fixed_int = im2_int(:,:,1);
% fixed_in_all = im2_in_all(:,:,1);
% fixed_in_opl = im2_in_opl(:,:,1);
% fixed_in_sup = im2_in_sup(:,:,1);
% fixed_in_int = im2_in_int(:,:,1);

h = fspecial('average', [2 2]);

registered_strips_all(:,:,1) = fixed_all;
registered_strips_opl(:,:,1) = fixed_opl;
registered_strips_sup(:,:,1) = fixed_sup;
registered_strips_int(:,:,1) = fixed_int;
% registered_strips_in_all(:,:,1) = fixed_in_all;
% registered_strips_in_opl(:,:,1) = fixed_in_opl;
% registered_strips_in_sup(:,:,1) = fixed_in_sup;
% registered_strips_in_int(:,:,1) = fixed_in_int;

registered_strips_all(registered_strips_all == 0) = NaN;
registered_strips_opl(registered_strips_opl == 0) = NaN;
registered_strips_sup(registered_strips_sup == 0) = NaN;
registered_strips_int(registered_strips_int == 0) = NaN;
% registered_strips_in_all(registered_strips_in_all == 0) = NaN;
% registered_strips_in_opl(registered_strips_in_opl == 0) = NaN;
% registered_strips_in_sup(registered_strips_in_sup == 0) = NaN;
% registered_strips_in_int(registered_strips_in_int == 0) = NaN;

% Make a copy of im2 to use for the registration loop, because the size of
% im3 will be modified for the overlap calculations, but we do not want to
% lose the information stored in im2.
im3_all = im2_all;
im3_opl = im2_opl;
im3_sup = im2_sup;
im3_int = im2_int;
% im3_in_all = im2_in_all;
% im3_in_opl = im2_in_opl;
% im3_in_sup = im2_in_sup;
% im3_in_int = im2_in_int;

% For the strips where rigid registration fails
imfailed_all = zeros(size(im3_all,1), size(im3_all,2));
imfailed_opl = zeros(size(im3_all,1), size(im3_all,2));
imfailed_sup = zeros(size(im3_all,1), size(im3_all,2));
imfailed_int = zeros(size(im3_all,1), size(im3_all,2));
% imfailed_in_all = zeros(size(im3_all,1), size(im3_all,2));
% imfailed_in_opl = zeros(size(im3_all,1), size(im3_all,2));
% imfailed_in_sup = zeros(size(im3_all,1), size(im3_all,2));
% imfailed_in_int = zeros(size(im3_all,1), size(im3_all,2));



%% Main loop
for i = 2:size(im3_all,3)
    
    % The fixed strip will be im2(:,:,1) regardless. Safe bet to assume that
    % the largest strip will have the largest area of overlap with another
    % strip. The problem is figuring out which strip will be the moving strip.
    % This will have to be done at every iteration of the registration loop,
    % because fixed will have changed. So, with every new fixed image,
    % calculate the regions of overlap w.r.t. every other strip, EXCEPT those
    % that are already a part of fixed.
    
    overlap = zeros(size(fixed_sup,1), size(fixed_sup,2), size(im3_sup,3));
    overlapAreas = zeros(1,size(im3_sup,3));
    
    % Create logical images that represent the amount of overlap of each
    % strip with respect to the fixed strip. Start the loop at ii = 2
    % because obviously the overlap of the fixed strip w.r.t itself would
    % be the highest, so we'll ignore that.
    for ii = 2:size(im3_sup,3)
        for jj = 1:size(fixed_sup,1)
            for kk = 1:size(fixed_sup,2)
                if fixed_sup(jj, kk) && im3_sup(jj, kk, ii) ~= 0
                    overlap(jj, kk, ii) = 1;
                end
            end
        end
    end
    
    % Calculate the areas of these logical images to determine which strip
    % has the largest area of overlap with the fixed image.
    for ii = 2:size(overlap,3)
        if sum(sum(overlap(:,:,ii))) == 0
            overlapAreas(1,ii) = 0;
            continue
        end
        overlapStats = regionprops(overlap(:,:,ii), 'Area');
        overlapAreas(1,ii) = overlapStats.Area;
    end
    
    % ind represents the strip number of the strip that has the maximum
    % overlap with the fixed image. Hence, if ind = 7, this means that
    % im2(:,:,7) has the largest overlap with the fixed image, and that
    % im2(:,:,7) should be the moving strip for this iteration.
    [~, ind] = max(overlapAreas);
    moving_all = im3_all(:,:,ind);
    moving_opl = im3_opl(:,:,ind);
    moving_sup = im3_sup(:,:,ind);
    moving_int = im3_int(:,:,ind);
%     moving_in_all = im3_in_all(:,:,ind);
%     moving_in_opl = im3_in_opl(:,:,ind);
%     moving_in_sup = im3_in_sup(:,:,ind);
%     moving_in_int = im3_in_int(:,:,ind);
    
    % Now delete this strip from im3 to prepare for the next iteration, as
    % this process will have to repeat again.
    im3_all(:,:,ind) = [];
    im3_opl(:,:,ind) = [];
    im3_sup(:,:,ind) = [];
    im3_int(:,:,ind) = [];
%     im3_in_all(:,:,ind) = [];
%     im3_in_opl(:,:,ind) = [];
%     im3_in_sup(:,:,ind) = [];
%     im3_in_int(:,:,ind) = [];
    

    %% Cross correlation registration
    
    moving_filt = mat2gray(double(moving_sup));
    moving_filt = imfilter(moving_filt,h);
    fixed_filt = mat2gray(double(fixed_sup));
    fixed_filt = imfilter(fixed_filt,h);
    
    %figure, imshowpair(fixed_sup, moving_sup)
    
    [output, ~] = dftregistration(fft2(double(fixed_filt)*255),fft2(double(moving_filt)*255),100);
    if (abs(round(output(3))) > 40 || abs(round(output(4))) > 100)
    else
        fedge  = find(mean(moving_sup),1);
        [~,I] = (sort((abs(diff([0 mean(moving_sup) 0]))),'descend'));
        I = I-1;
        if 0 > fedge + round(output(4))
            moving_all = circshift(circshift(moving_all,round(output(4)),2),round(output(3)),1);
            moving_opl = circshift(circshift(moving_opl,round(output(4)),2),round(output(3)),1);
            moving_sup = circshift(circshift(moving_sup,round(output(4)),2),round(output(3)),1);
            moving_int = circshift(circshift(moving_int,round(output(4)),2),round(output(3)),1);
%             moving_in_all = circshift(circshift(moving_in_all,round(output(4)),2),round(output(3)),1);
%             moving_in_opl = circshift(circshift(moving_in_opl,round(output(4)),2),round(output(3)),1);
%             moving_in_sup = circshift(circshift(moving_in_sup,round(output(4)),2),round(output(3)),1);
%             moving_in_int = circshift(circshift(moving_in_int,round(output(4)),2),round(output(3)),1);
            
            moving_all(:,size(moving_all,2)-abs(fedge + round(output(4))):size(moving_all,2)) = 0;
            moving_opl(:,size(moving_all,2)-abs(fedge + round(output(4))):size(moving_all,2)) = 0;
            moving_sup(:,size(moving_all,2)-abs(fedge + round(output(4))):size(moving_all,2)) = 0;
            moving_int(:,size(moving_all,2)-abs(fedge + round(output(4))):size(moving_all,2)) = 0;
%             moving_in_all(:,size(moving_all,2)-abs(fedge + round(output(4))):size(moving_all,2)) = 0;
%             moving_in_opl(:,size(moving_all,2)-abs(fedge + round(output(4))):size(moving_all,2)) = 0;
%             moving_in_sup(:,size(moving_all,2)-abs(fedge + round(output(4))):size(moving_all,2)) = 0;
%             moving_in_int(:,size(moving_all,2)-abs(fedge + round(output(4))):size(moving_all,2)) = 0;
            
        elseif round(output(4)) == 0 || (max(I(1:2)) + abs(round(output(4)))) < size(moving_all,2)
            moving_all = circshift(circshift(moving_all,round(output(4)),2),round(output(3)),1);
            moving_opl = circshift(circshift(moving_opl,round(output(4)),2),round(output(3)),1);
            moving_sup = circshift(circshift(moving_sup,round(output(4)),2),round(output(3)),1);
            moving_int = circshift(circshift(moving_int,round(output(4)),2),round(output(3)),1);
%             moving_in_all = circshift(circshift(moving_in_all,round(output(4)),2),round(output(3)),1);
%             moving_in_opl = circshift(circshift(moving_in_opl,round(output(4)),2),round(output(3)),1);
%             moving_in_sup = circshift(circshift(moving_in_sup,round(output(4)),2),round(output(3)),1);
%             moving_in_int = circshift(circshift(moving_in_int,round(output(4)),2),round(output(3)),1);
            
        else
            moving_all = circshift(circshift(moving_all,round(output(4)),2),round(output(3)),1);
            moving_opl = circshift(circshift(moving_opl,round(output(4)),2),round(output(3)),1);
            moving_sup = circshift(circshift(moving_sup,round(output(4)),2),round(output(3)),1);
            moving_int = circshift(circshift(moving_int,round(output(4)),2),round(output(3)),1);
%             moving_in_all = circshift(circshift(moving_in_all,round(output(4)),2),round(output(3)),1);
%             moving_in_opl = circshift(circshift(moving_in_opl,round(output(4)),2),round(output(3)),1);
%             moving_in_sup = circshift(circshift(moving_in_sup,round(output(4)),2),round(output(3)),1);
%             moving_in_int = circshift(circshift(moving_in_int,round(output(4)),2),round(output(3)),1);
            
            moving_all(:,1:abs(size(im1_all,2)-abs(max(I(1:2)) + abs(round(output(4)))))) = 0;
            moving_opl(:,1:abs(size(im1_all,2)-abs(max(I(1:2)) + abs(round(output(4)))))) = 0;
            moving_sup(:,1:abs(size(im1_all,2)-abs(max(I(1:2)) + abs(round(output(4)))))) = 0;
            moving_int(:,1:abs(size(im1_all,2)-abs(max(I(1:2)) + abs(round(output(4)))))) = 0;
%             moving_in_all(:,1:abs(size(im1_all,2)-abs(max(I(1:2)) + abs(round(output(4)))))) = 0;
%             moving_in_opl(:,1:abs(size(im1_all,2)-abs(max(I(1:2)) + abs(round(output(4)))))) = 0;
%             moving_in_sup(:,1:abs(size(im1_all,2)-abs(max(I(1:2)) + abs(round(output(4)))))) = 0;
%             moving_in_int(:,1:abs(size(im1_all,2)-abs(max(I(1:2)) + abs(round(output(4)))))) = 0;
            
        end
        if 0 < round(output(3))
            %im1(:,:,i) = (abs(ifft2(imraw2))/(max(max(abs(ifft2(imraw2))))))*255;
            moving_all(1:abs(round(output(3))),:) = 0;
            moving_opl(1:abs(round(output(3))),:) = 0;
            moving_sup(1:abs(round(output(3))),:) = 0;
            moving_int(1:abs(round(output(3))),:) = 0;
%             moving_in_all(1:abs(round(output(3))),:) = 0;
%             moving_in_opl(1:abs(round(output(3))),:) = 0;
%             moving_in_sup(1:abs(round(output(3))),:) = 0;
%             moving_in_int(1:abs(round(output(3))),:) = 0;
            
        elseif round(output(3)) < 0
            % im1(:,:,i) = (abs(ifft2(imraw2))/(max(max(abs(ifft2(imraw2))))))*255;
            moving_all(size(moving_all,1)-abs(round(output(3))):end,:) = 0;
            moving_opl(size(moving_all,1)-abs(round(output(3))):end,:) = 0;
            moving_sup(size(moving_all,1)-abs(round(output(3))):end,:) = 0;
            moving_int(size(moving_all,1)-abs(round(output(3))):end,:) = 0;
%             moving_in_all(size(moving_all,1)-abs(round(output(3))):end,:) = 0;
%             moving_in_opl(size(moving_all,1)-abs(round(output(3))):end,:) = 0;
%             moving_in_sup(size(moving_all,1)-abs(round(output(3))):end,:) = 0;
%             moving_in_int(size(moving_all,1)-abs(round(output(3))):end,:) = 0;
        end
        
    end
    
    %figure, imshowpair(fixed_sup, moving_sup)
    %close all

    fixed_all = mat2gray(fixed_all);
    fixed_opl = mat2gray(fixed_opl);
    fixed_sup = mat2gray(fixed_sup);
    fixed_int = mat2gray(fixed_int);
%     fixed_in_all = mat2gray(fixed_in_all);
%     fixed_in_opl = mat2gray(fixed_in_opl);
%     fixed_in_sup = mat2gray(fixed_in_sup);
%     fixed_in_int = mat2gray(fixed_in_int);
    
    %% Rigid Registration
    
%     [im5_all(:,:,i), im5_opl(:,:,i), im5_sup(:,:,i), im5_int(:,:,i), ...
%         im5_in_all(:,:,i), im5_in_opl(:,:,i), im5_in_sup(:,:,i), im5_in_int(:,:,i)] = ...
%         BRISK_SURF_GEN(fixed_all, moving_all, moving_opl, moving_sup, moving_int, ...
%         moving_in_all, moving_in_opl, moving_in_sup, moving_in_int);
    
      [im5_all(:,:,i), im5_opl(:,:,i), im5_sup(:,:,i), im5_int(:,:,i)] = ...
        BRISK_SURF_GEN(fixed_all, moving_all, moving_opl, moving_sup, moving_int);
    
    if sum(sum(im5_all(:,:,i))) == 0
        imfailed_all(:,:,size(imfailed_all,3)+1) = moving_all;
        imfailed_opl(:,:,size(imfailed_opl,3)+1) = moving_opl;
        imfailed_sup(:,:,size(imfailed_sup,3)+1) = moving_sup;
        imfailed_int(:,:,size(imfailed_int,3)+1) = moving_int;
%         imfailed_in_all(:,:,size(imfailed_in_all,3)+1) = moving_in_all;
%         imfailed_in_opl(:,:,size(imfailed_in_opl,3)+1) = moving_in_opl;
%         imfailed_in_sup(:,:,size(imfailed_in_sup,3)+1) = moving_in_sup;
%         imfailed_in_int(:,:,size(imfailed_in_int,3)+1) = moving_in_int;
    end
        
    
    %% Fixed strip re-creation
    
    %%% New method of creating the fixed strip: Take only the part of the
    %%% moving strip that does not overlap with the previous fixed strip,
    %%% and 'append' it to the previous fixed strip to make the new fixed
    %%% strip. This avoids repeated averaging of overlapping areas, which
    %%% would cause the fixed strip to be different every time. Also would
    %%% mess up sassie, cause sassie is not meant to operate on
    %%% already-averaged images
    
    %%% BOIIIIII
    %%% Turns out the below code was causing so many issues. Including the
    %%% supposed 'contrast variation' causing the 'ghost strip' illusion (although this might also be caused by averaging).
    %%% I forgot the fact that some internal values of each strip will be
    %%% equal to zero. Thus I was replacing internal fixed strip values
    %%% with moving strip values. So to fix this, I create a temporary
    %%% strip which is averaged. This removes the internal zeros (or the
    %%% vast majority of them). 
    %%% This also seems to deal with the black lines (finally...). I
    %%% replace the values in the fixed strip not just where they are zero,
    %%% but in a 3x3 square around each pixel, ensuring no pixels get
    %%% missed.
    temp_all = fixed_all;
    temp_all = imfilter(temp_all, h);
    for ii = 2:size(fixed_all, 1)-1
        for jj = 2:size(fixed_all, 2)-1
            
            % If the fixed strip is zero at a point and the moving strip is
            % not zero at that point, set the fixed strip pixel equal
            % to the moving strip pixel. 
            if temp_all(ii, jj) == 0 && im5_all(ii, jj, i) ~= 0
                fixed_all(ii-1:ii+1, jj-1:jj+1) = im5_all(ii-1:ii+1, jj-1:jj+1, i);
                fixed_opl(ii-1:ii+1, jj-1:jj+1) = im5_opl(ii-1:ii+1, jj-1:jj+1, i);
                fixed_sup(ii-1:ii+1, jj-1:jj+1) = im5_sup(ii-1:ii+1, jj-1:jj+1, i);
                fixed_int(ii-1:ii+1, jj-1:jj+1) = im5_int(ii-1:ii+1, jj-1:jj+1, i);
%                 fixed_in_all(ii-1:ii+1, jj-1:jj+1) = im5_in_all(ii-1:ii+1, jj-1:jj+1, i);
%                 fixed_in_opl(ii-1:ii+1, jj-1:jj+1) = im5_in_opl(ii-1:ii+1, jj-1:jj+1, i);
%                 fixed_in_sup(ii-1:ii+1, jj-1:jj+1) = im5_in_sup(ii-1:ii+1, jj-1:jj+1, i);
%                 fixed_in_int(ii-1:ii+1, jj-1:jj+1) = im5_in_int(ii-1:ii+1, jj-1:jj+1, i);
%                 fixed_all(ii, jj) = im5_all(ii,jj,i);
%                 fixed_opl(ii, jj) = im5_opl(ii,jj,i);
%                 fixed_sup(ii, jj) = im5_sup(ii,jj,i);
            end
        end
    end
    
    %figure, imshowpair(fixed_all, im5_all(:,:,i))
    
    %Save in case it crashes
    %save(fullfile(loadloc, 'strips_Registration.mat'))
end

toc

% temp = im5_sup;
% temp(:,:,1) = registered_strips_sup(:,:,1);
% temp(temp == 0) = NaN;
% C = zeros([size(temp,1) size(temp,2)]);
% for i = 1:size(temp,1)
%     for j = 1:size(temp,2)
%         A = num2cell(squeeze(temp(i,j,:)));
%         B = A(find(cellfun(@(x)~isnan(x), A) == 1, 1));
%         if isempty(cell2mat(B)) == 0
%             C(i,j) = cell2mat(B);
%         end
%     end
% end

% Delete any blank frames
ii = 1;
while ii <= size(im5_sup,3)
    if sum(sum(im5_sup(:,:,ii))) == 0
        im5_all(:,:,ii) = [];
        im5_opl(:,:,ii) = [];
        im5_sup(:,:,ii) = [];
        im5_int(:,:,ii) = [];
%         im5_in_all(:,:,ii) = [];
%         im5_in_opl(:,:,ii) = [];
%         im5_in_sup(:,:,ii) = [];
%         im5_in_int(:,:,ii) = [];
        
        
        ii = ii - 1;
    end
    ii = ii + 1;
end


%% SaSSIE
save(fullfile(loadloc, 'strips_postRegistration.mat'))

%registered_strips_opl2 = zeros(size(registered_strips_opl));

% Perform SaSSIE on these manually registered strips
for i = 1:size(im5_sup,3)
    
    moving_all = im5_all(:,:,i);
    moving_opl = im5_opl(:,:,i);
    moving_sup = im5_sup(:,:,i);
    moving_int = im5_int(:,:,i);
%     moving_in_all = im5_in_all(:,:,i);
%     moving_in_opl = im5_in_opl(:,:,i);
%     moving_in_sup = im5_in_sup(:,:,i);
%     moving_in_int = im5_in_int(:,:,i);
    
    tic
    %[moving_all, moving_opl2, moving_sup] = SaSSIE_Arman_v6(fixed_sup, moving_all, moving_opl, moving_sup);
    %[moving_opl2] = SaSSIE_Arman_v6_OPL(fixed_opl, moving_opl); % SaSSIE's the OPL by itself.
%     [moving_all, moving_opl, moving_sup, moving_int, ...
%         moving_in_all, moving_in_opl, moving_in_sup, moving_in_int] = ...
%         SaSSIE_GEN(fixed_all, moving_all, moving_opl, moving_sup, moving_int, ...
%         moving_in_all, moving_in_opl, moving_in_sup, moving_in_int);
    
        [moving_all, moving_opl, moving_sup, moving_int ] = SaSSIE_GEN(fixed_all, moving_all, moving_opl, moving_sup, moving_int);
    toc
    
    % Set all zero values to NaN to be able to use nanmean to average
    moving_all(moving_all == 0) = NaN;
    moving_opl(moving_opl == 0) = NaN;
    moving_sup(moving_sup == 0) = NaN;
    moving_int(moving_int == 0) = NaN;
    
%     moving_in_all(moving_in_all == 0) = NaN;
%     moving_in_opl(moving_in_opl == 0) = NaN;
%     moving_in_sup(moving_in_sup == 0) = NaN;
%     moving_in_int(moving_in_int == 0) = NaN;
    %moving_opl2(moving_opl2 == 0) = NaN;
    
    % Store the registered moving strips. The nanmean of these become the
    % final image.
    registered_strips_all(:,:,size(registered_strips_all,3)+1) = moving_all;
    registered_strips_opl(:,:,size(registered_strips_opl,3)+1) = moving_opl;
    registered_strips_sup(:,:,size(registered_strips_sup,3)+1) = moving_sup;
    registered_strips_int(:,:,size(registered_strips_int,3)+1) = moving_int;
%     registered_strips_in_all(:,:,size(registered_strips_in_all,3)+1) = moving_in_all;
%     registered_strips_in_opl(:,:,size(registered_strips_in_opl,3)+1) = moving_in_opl;
%     registered_strips_in_sup(:,:,size(registered_strips_in_sup,3)+1) = moving_in_sup;
%     registered_strips_in_int(:,:,size(registered_strips_in_int,3)+1) = moving_in_int;
    %registered_strips_opl2(:,:,size(registered_strips_opl2,3)+1) = moving_opl2;
    
    save(fullfile(loadloc, 'strips_SaSSIE.mat'))
    
end

toc

save(fullfile(loadloc, 'strips_postSaSSIE.mat'))

% % % Delete any blank frames
% % ii = 1;
% % while ii <= size(registered_strips_all,3)
% %     if sum(sum(registered_strips_all(:,:,ii))) == 0
% %         registered_strips_all(:,:,ii) = [];
% %         registered_strips_opl(:,:,ii) = [];
% % %         registered_strips_opl2(:,:,ii) = [];
% %         registered_strips_sup(:,:,ii) = [];
% %         ii = ii - 1;
% %     end
% %     ii = ii + 1;
% % end

% % Threshold the registered strips to get rid of black lines. Idea is if at
% % least most of the black lines become NaN, they will be filled in during
% % nanmean by strips that cover the black line
% for ii = 2:size(registered_strips_all,3)
%     temp_all = registered_strips_all(:,:,ii);
%     temp_opl = registered_strips_opl(:,:,ii);
% %     temp_opl2 = registered_strips_opl2(:,:,ii);
%     temp_sup = registered_strips_sup(:,:,ii);
%     temp_all(temp_all < 0.1) = NaN;
%     temp_opl(temp_opl < 0.1) = NaN;
%     temp_sup(temp_sup < 0.1) = NaN;
%     registered_strips_all(:,:,ii) = temp_all;
%     registered_strips_opl(:,:,ii) = temp_opl;
% %     registered_strips_opl2(:,:,ii) = temp_opl2;
%     registered_strips_sup(:,:,ii) = temp_sup;
% end


% % Projection Artefact Removal: it works!
% testopl = ((squeeze(nanmean(final_opl,3))).*(1-mat2gray((squeeze(nanmean(final_sup,3))))));

final_all = nanmean(registered_strips_all,3);
final_opl = nanmean(registered_strips_opl,3);
final_sup = nanmean(registered_strips_sup,3);
final_int = nanmean(registered_strips_int,3);
% final_in_all = nanmean(registered_strips_in_all,3);
% final_in_opl = nanmean(registered_strips_in_opl,3);
% final_in_sup = nanmean(registered_strips_in_sup,3);
% final_in_int = nanmean(registered_strips_in_int,3);
%final_opl2 = nanmean(registered_strips_opl2,3);

final_all(isnan(final_all)) = 0;
final_opl(isnan(final_opl)) = 0;
final_sup(isnan(final_sup)) = 0;
final_int(isnan(final_int)) = 0;
% final_in_all(isnan(final_in_all)) = 0;
% final_in_opl(isnan(final_in_opl)) = 0;
% final_in_sup(isnan(final_in_sup)) = 0;
% final_in_int(isnan(final_in_int)) = 0;
%final_opl2(isnan(final_opl2)) = 0;

final_all = imsharpen(imadjust(final_all, [0.18 0.75]));
final_opl = imsharpen(imadjust(final_opl, [0.18 0.75]));
final_sup = imsharpen(imadjust(final_sup, [0.18 0.75]));
final_int = imsharpen(imadjust(final_int, [0.18 0.75]));
% final_in_all = imsharpen(imadjust(final_in_all, [0.18 0.75]));
% final_in_opl = imsharpen(imadjust(final_in_opl, [0.18 0.75]));
% final_in_sup = imsharpen(imadjust(final_in_sup, [0.18 0.75]));
% final_in_int = imsharpen(imadjust(final_in_int, [0.18 0.75]));
% % final_opl2 = imsharpen(imadjust(final_opl2));


save(fullfile(loadloc, 'strips_final.mat'))

% Create and store the final images.
imwrite(final_all, fullfile(loadloc,'final_ALL.tif'),'tiff','Compression','none','Resolution', 900);
imwrite(final_opl, fullfile(loadloc,'final_OPL.tif'),'tiff','Compression','none','Resolution', 900);
imwrite(final_sup, fullfile(loadloc,'final_SUP.tif'),'tiff','Compression','none','Resolution', 900);
imwrite(final_int, fullfile(loadloc,'final_INT.tif'),'tiff','Compression','none','Resolution', 900);
% imwrite(final_in_all, fullfile(loadloc,'final_IN_ALL.tif'),'tiff','Compression','none','Resolution', 900);
% imwrite(final_in_opl, fullfile(loadloc,'final_IN_OPL.tif'),'tiff','Compression','none','Resolution', 900);
% imwrite(final_in_sup, fullfile(loadloc,'final_IN_SUP.tif'),'tiff','Compression','none','Resolution', 900);
% imwrite(final_in_int, fullfile(loadloc,'final_IN_INT.tif'),'tiff','Compression','none','Resolution', 900);


%imwrite(final_opl2, fullfile(loadloc,'final_OPL2.tif'),'tiff','Compression','none','Resolution', 900);

end
