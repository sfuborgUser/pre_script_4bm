function corrAxialMotion(fn,volume)
%% Compute maximum cross-correlation (axial only)
volLog = 20*log10(abs(volume));
motionA = maxxcorrAx(volLog);
xaxis = [1:1:size(motionA,2)];
bscan = volume(:,:,10);
ex = squeeze(volume(:,30,:));

%% Set smoothing parameter
p = polyfit(xaxis,motionA,2);
f = polyval(p,xaxis);

%% Compute motion correction parameters and do motion correction
disp_ind = motionA - f;
[m n] = size(ex);
topZero = max(disp_ind);
botZero = abs(min(disp_ind));
for k=1:n
    top = round(topZero-disp_ind(k));
    volume_mcorr(top+1:top+m,:,k) = volume(:,:,k);
end

%% Crop
cropOff = topZero+botZero;
volume_mcorr(1:cropOff,:,:) = [];
volume_mcorr(end-cropOff+1:end,:,:) = [];

%% Save
if ispc
    mcorrdir = fullfile('/ensc/IMAGEBORG/DATA/ECC/mcorr',fn(1:4))
else
    mcorrdir = fullfile('/ensc/IMAGEBORG/DATA/ECC/mcorr',fn(1:4))
end
dirc = fn(1:4);
savepath = fullfile(mcorrdir);
if exist(savepath)
    savepath = savepath;
else
    mkdir(savepath);
end
save(fullfile(savepath,[fn,'    _mcorr']), 'volume_mcorr', '-v7.3');
end