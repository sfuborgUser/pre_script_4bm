function gen_from_datalist(list)
%This is typed into the command line
%This function generates the the files that are necessary to be submitted
%by using ./ALL_JOBS_SUBMIT_etc..etc..etc..
%That command will run the AO_pre_script along with several other commands
%written here in the file
addpath('/ensc/IMAGEBORG/DATA/ECC/scripts/final/OCT');
fid = fopen(list);
tList = textscan(fid,'%s');
files = char(tList{1});
addpath(genpath('/ensc/IMAGEBORG/DATA/ECC/scripts/final_AO/final/OCT'))

commandString1 = '#PBS -W group_list=ensc-imageborg';
commandString2 = '#PBS -l nodes=1:ppn=1';
commandString3 = '#PBS -l walltime=23:00:00';
commandString4 = '#PBS -l pmem=26gb';
commandString5 = '#PBS -m bea';
commandString6 = 'cd /ensc/IMAGEBORG/DATA/ECC/scripts/final/OCT';
%PR = 1;
%OCTA = 0;

for i = 1:size(files,1)
    fileName = strtrim(files(i,:));
    fopen(fileName,'w+');
    fclose('all');
    fileNameFull = fullfile('/ensc/IMAGEBORG/DATA/ECC/scripts/final/OCT',fileName);
    fileID = fopen(fileName,'w');
    commandString = ['matlab -nodisplay -nodesktop -r "Pre_script(''',strtrim(files(i,:)),'''); exit"'];
    fprintf(fileID,'%s\n',commandString1);
    fprintf(fileID,'%s\n',commandString2);
    fprintf(fileID,'%s\n',commandString3);
    fprintf(fileID,'%s\n',commandString4);
    fprintf(fileID,'%s\n',commandString5);
    fprintf(fileID,'%s\n',commandString6);
    fprintf(fileID,'%s\n',commandString);
    fclose(fileID);   
end

allJobsFileName = '/ensc/IMAGEBORG/DATA/ECC/scripts/final/OCT/ALL_JOBS_SUBMIT_LIST_AO.sh';
fileID_ALL_JOBS = fopen(allJobsFileName,'w');
for i = 1:size(files,1)
    fileName = strtrim(files(i,:));
    fprintf(fileID_ALL_JOBS,'%s\n',['qsub ',fileName]);
end
fclose(fileID_ALL_JOBS);


end



