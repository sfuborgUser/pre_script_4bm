
subj = 'H866';
date = '2019.01.21';
allJobsFileName = '/ensc/IMAGEBORG/DATA/ECC/scripts/final/Demons/ALL_JOBS_SUBMIT_LIST_AO.sh';


commandString1 = '#PBS -W group_list=ensc-imageborg';
commandString2 = '#PBS -l nodes=1:ppn=1';
commandString3 = '#PBS -l walltime=23:00:00';
commandString4 = '#PBS -l pmem=26gb';
commandString5 = '#PBS -m bea';
commandString6 = 'cd /ensc/IMAGEBORG/STUDENTS/FRANCIS/GitRepo/Pre_script_4BM';


cd(fullfile('/ensc/IMAGEBORG/DATA/ECC/fundus/', subj, date));

folders = dir;



for i = 3:size(folders, 1)
	
	cd(folders(i).name);
	
	sSize = dir;
	
	for k = 3:size(sSize, 1)
		
		cd(sSize(k).name);
		
		mlist = dir('*.tif');

		for j = 1:size(mlist, 1)

			fclose('all');
			fileName = mlist(j).name;
	% %         fileName = FileList(i).name;
	% %         fileName = fileName(1:end-4)
	%         fopen(fileName(1:end-14),'w+');
			fclose('all');
			fileNameFull = fullfile('/ensc/IMAGEBORG/DATA/ECC/scripts/final/Demons/',fileName(1:end-8));
			fileID = fopen(fileNameFull,'w');
			
			commandString = ...
			['matlab -nodisplay -nodesktop -r "FTdemons(' '''',...
			strrep(sSize(k).folder, 'fundus', 'mcorr'),'''' ', ''',...
			fileName(1:end-8),''', ''' ,sSize(k).name,'''); exit"'];
		
			fprintf(fileID,'%s\n',commandString1);
			fprintf(fileID,'%s\n',commandString2);
			fprintf(fileID,'%s\n',commandString3);
			fprintf(fileID,'%s\n',commandString4);
			fprintf(fileID,'%s\n',commandString5);
			fprintf(fileID,'%s\n',commandString6);
			fprintf(fileID,'%s\n',commandString);
			fclose(fileID);   

		end
		cd('..');
		
		fileID_ALL_JOBS = fopen(allJobsFileName, 'a');
		% fileID_ALL_JOBS = fopen(allJobsFileName, 'w+');
		for r = 1:size(mlist,1)
			fileName = mlist(r).name;
			fprintf(fileID_ALL_JOBS,'%s\n',['qsub ',fileName(1:end-8)]);
		end
		fclose(fileID_ALL_JOBS);
		
	end
	
	cd('..');
	
	
end




