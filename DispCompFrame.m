function dispersionComepensatedFrame = DispCompFrame(rawFrame, dispCoeff)

ScanPts         = size(rawFrame, 1);
LinePerFrame    = size(rawFrame, 2);
kLinear        = -ScanPts/2+1:-ScanPts/2+ScanPts;
kaxis          = repmat(kLinear',1,LinePerFrame) - 1;

rawFrame = hilbert(rawFrame);
ph = angle(raw_fr)

for i = 1 : size(dispCoeff, 2)
    ph = ph + (dispCoeff(i)*(1e-3^(i+1)).*(kaxis)^2);
end

dispersionComepensatedFrame = abs(raw_fr).*exp(1i.*ph);


end

