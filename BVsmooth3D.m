function V = BVsmooth3D(newVolume,dsRate,lam,Nsteps)
%%
% loadvol: 
% Nsteps: number of steps 
% References 
% - Mathematical Image Analysis by Laurent Younes, pg. 30 
% - BVsmooth.m by Faisal Beg (2D BV smoothing)

%subjects = ['H075-OD-Vol-4'; 'H075-OS-Vol-2'; 'H076-OD-Vol-2'; 'H076-OS-Vol-3'; 'H099-OS-Vol-3'; 'H099-OD-Vol-3'] 
%subjects = ['H106-OD-Vol-1'; 'H106-OS-Vol-4'; 'H120-OD-Vol-4'; 'H120-OS-Vol-8']; 


% % parameters
eps = 1e-7;
%lam = 0.001;
h=1; dx=h; dy=h; dz=h;
dt = min(0.5*h/lam, h^2/6);
%Nsteps = 16;
%dsRate = 1;


% V0: downsample (temp) and mat2gray
V0 = mat2gray(newVolume(1:dsRate:end,1:dsRate:end,1:dsRate:end)); clear newVolume
% V0 = mat2gray(double(V0));
% V0 = mat2gray(newVolume);

% initialize V    
V = zeros(size(V0)+2);
V(2:end-1, 2:end-1, 2:end-1) = V0;

for i=1:Nsteps
    tic
    % boundary padding: to satisfy dot(del(I),N)=0  
    V(1,2:end-1,2:end-1) = V(2,2:end-1,2:end-1);
    V(end,2:end-1,2:end-1) = V(end-1,2:end-1,2:end-1);
    V(2:end-1,1,2:end-1) = V(2:end-1,2,2:end-1);
    V(2:end-1,end,2:end-1) = V(2:end-1,end-1,2:end-1);
    V(2:end-1,2:end-1,1) = V(2:end-1,2:end-1,2);
    V(2:end-1,2:end-1,end) = V(2:end-1,2:end-1,end-1);

    % Discretized derivatives 
    Vx = (V(3:end, 2:end-1, 2:end-1) - V(1:end-2, 2:end-1, 2:end-1))/(2*dx); 
    Vy = (V(2:end-1, 3:end, 2:end-1) - V(2:end-1, 1:end-2, 2:end-1))/(2*dy);
    Vz = (V(2:end-1, 2:end-1, 3:end) - V(2:end-1, 2:end-1, 1:end-2))/(2*dz);
    G = sqrt(Vx.^2 + Vy.^2 + Vz.^2);

    Vxx = (V(3:end, 2:end-1, 2:end-1) - 2*V(2:end-1, 2:end-1, 2:end-1) + V(1:end-2, 2:end-1, 2:end-1))/(dx*dx); 
    Vxy = (V(3:end, 3:end, 2:end-1) - V(1:end-2, 3:end, 2:end-1) - V(3:end, 1:end-2, 2:end-1) + V(1:end-2, 1:end-2, 2:end-1))/(4*dx*dy);
    Vxz = (V(3:end, 2:end-1, 3:end) - V(1:end-2, 2:end-1, 3:end) - V(3:end, 2:end-1, 1:end-2) + V(1:end-2, 2:end-1, 1:end-2))/(4*dx*dz); 
    X = Vxx./(G+eps) - Vx.*(Vx.*Vxx + Vy.*Vxy + Vz.*Vxz)./((G.^2)+eps^2).^(3/2); clear Vxx;

    Vyy = (V(2:end-1, 3:end, 2:end-1) - 2*V(2:end-1, 2:end-1, 2:end-1) + V(2:end-1, 1:end-2, 2:end-1))/(dy*dy);
    Vyz = (V(2:end-1, 3:end, 3:end) - V(2:end-1, 1:end-2, 3:end) - V(2:end-1, 3:end, 1:end-2) + V(2:end-1, 1:end-2, 1:end-2))/(4*dy*dz);
    Y = Vyy./(G+eps) - Vy.*(Vx.*Vxy + Vy.*Vyy + Vz.*Vyz)./((G.^2)+eps^2).^(3/2); clear Vyy Vxy;
    XplusY = X + Y; clear X Y;

    Vzz = (V(2:end-1, 2:end-1, 3:end) - 2*V(2:end-1, 2:end-1, 2:end-1) + V(2:end-1, 2:end-1, 1:end-2))/(dz*dz);
    Z = Vzz./(G+eps) - Vz.*(Vx.*Vxz + Vy.*Vyz + Vz.*Vzz)./((G.^2)+eps^2).^(3/2); clear Vzz Vxz Vyz Vx Vy Vz;

    % Gradient descent equation  
    % G: magnitude of gradient

    % D: bracketed term 
    div = XplusY + Z; clear X Y Z;
    B = zeros(size(V0)+2);
    B(2:end-1, 2:end-1, 2:end-1) = V(2:end-1, 2:end-1, 2:end-1) - V0; 
    D = div - 2 * lam * B(2:end-1, 2:end-1, 2:end-1); clear div B;

    V(2:end-1, 2:end-1, 2:end-1) = V(2:end-1, 2:end-1, 2:end-1) + dt * G.* D;
    toc
%    pause;
end

V = V(2:end-1,2:end-1,2:end-1);

% typecast to int16
V = -32768+ (2*32768)/(max(V(:))-min(V(:)))*(V-min(V(:)));
V = int16(V);


