folder = 'D:\DATA\CC';
addpath('D:\MJ\Dropbox\ProgramScripts\MatlabScripts\OCTViewer_Project');

rescaleFolder = 'D:\DATA\CC';
fn_ResParam = 'LUTSS.bin';
fid_ResParam = fopen(fullfile(rescaleFolder,fn_ResParam));
rescaleParam = fread(fid_ResParam, 'double')+1.00;
LUT =  rescaleParam;
fclose(fid_ResParam);

dispMaxOrder    = 5;
coeffRange      = 50;

%%%%% Find filenames of RAW files to be processed %%%%%
cd(folder);
files   = (dir('*.unp'));
fnames  = {files.name}';
fn = fnames{4};

%%% Load Acquisition Parameters %%%
parameters    = getParameters(fn);
numPoints     = parameters(1);
numAscans     = parameters(2);
numBscans     = parameters(3);
numCscans     = parameters(4);
numMscans     = parameters(5);

%%% File open %%%
fid = fopen(fn);

ref_Frame     = 150;
fseek(fid,2*numPoints*numAscans*(ref_Frame-1),-1);
ref_RawData  = fread(fid,[numPoints,numAscans], 'uint16');
ref_FFTData  = fft(hilbert(ref_RawData));
ref_Aline    = ref_FFTData(:,end/2);

offSetIdx  = 15;
diffPhases = estPhaseDiff(ref_Aline, ref_FFTData, offSetIdx);

ref_RawData_2 = ifft(ref_FFTData);

for k = 1:size(ref_RawData_2,2)
    ref_RawData_3(:,k) = ref_RawData_2(:,k).*exp(-1j*(diffPhases(k)));
end

ref_FFTData_2      = fft(ref_RawData_3);
ref_Img            = reSampling(ref_FFTData_2, LUT);
ref_img_FPNSub     = ref_Img...
        - (repmat(median(real(ref_Img),2), [1,size(ref_Img,2)])...
        +1j.*repmat(median(imag(ref_Img),2), [1,size(ref_Img,2)]));

ref_Img_HamWin     = ref_img_FPNSub.*repmat(hann(size(ref_img_FPNSub,1)),[1 size(ref_img_FPNSub,2)]);       
dispCoeffs         = setDispCoeff(ref_Img_HamWin, dispMaxOrder, coeffRange);

% ref_img_DispComp   = compDisPhase(ref_Img_HamWin,dispMaxOrder,dispCoeffs);
% ref_FFT = fft(ref_img_DispComp);
% 
% figure
% ref_OCT = 20.*log10(abs(ref_FFT(50:200,:)));
% imagesc(imadjust(mat2gray(ref_OCT))); colormap(gray)


for FrameNum = 1:numBscans    
   %%% Load raw spectrum %%%
    fseek(fid,2*numPoints*numAscans*(FrameNum-1),-1);
    rawData = fread(fid,[numPoints,numAscans], 'uint16');
    fftData = fft(hilbert(rawData));
    %%% Estimating spectral-shift amount in sub-pixel %%%
    diffPhases = estPhaseDiff(ref_Aline, fftData, offSetIdx);
    rawData_2 = ifft(fftData);
    
    for k = 1:size(rawData_2,2)
        rawData_3(:,k) = rawData_2(:,k).*exp(-1j*(diffPhases(k)));
    end
    
    fftData2   = fft(rawData_3);
    img        = reSampling(fftData2, LUT);

    %%% DC & Fixed pattern noise remove process through medial filtering %%%
    img_FPNSub    = img...
        - (repmat(median(real(img),2), [1,size(img,2)])...
        +1j.*repmat(median(imag(img),2), [1,size(img,2)]));
    
    %%% Windowing process %%%
    img_HamWin    = img_FPNSub.*repmat(hann(size(img_FPNSub,1)),[1 size(img_FPNSub,2)]);

    %%% Dispersion compensation with the estimated dispersion coefficients %%%
    img_DispComp  = compDisPhase(img_HamWin,dispMaxOrder,dispCoeffs);

    %%% FFT %%%
    img_FFT                      = fft(img_DispComp);
    img_FFT_ROI                  = img_FFT(1:end/2,:);
    ProcdData_temp(:,:,FrameNum) = img_FFT_ROI;
end


