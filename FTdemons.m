function FTdemons(mcorrpath, fname, ScanSize)

cd(mcorrpath);
mlist = dir('*.mat');

% Create reference volume 
% Just take the first volume you find
ref = load(mlist(1).name);

% Load volume
load(strcat(fname, '    _mcorr.mat'));

% Use DEMONSSSSS
N = 100;
[D, reg] = imregdemons(volume_mcorr, ref.volume_mcorr, N);

if ~exist(fullfile(ScanSize,'demons'), 'dir')
	mkdir(fullfile(ScanSize,'demons'));
end

save(fullfile(ScanSize, 'demons', strcat(fname(1:end-4), 'dem.mat')), 'reg', '-V7.3');

end