%% Phase Correlation
% m = MOVING IMAGE
% t = TEMPLATE IMAGE
% n = NON-RIGID REGISTERED (TRANSLATED) IMAGE

m = imnoise(m, 'gaussian', -.55, .0001); % imnoise(image, type, mean, stddev)
t = imnoise(t, 'gaussian', -.55, .0001); % Depending on type of image, alter mean and stddev 
                                         % - these parametres work well for Sup

R = ifft2((fft2(t).*conj(fft2(m)))./abs(fft2(t).*conj(fft2(m)))); % Phase Correlation equation
R = imgaussfilt(R,2); % Gaussian Filter of R with sigma=2, same as imnoise() play with sigma depending on output (2 works well for Sup)
[ro, co] = find(R==max(max(R))); % argmax(R)
ro = ro-1;
co = co-1;
dx = co-size(m,2)*round(co/size(m,2)); % Centreing phase shift (if you need explanation let me know)
dy = ro-size(m,1)*round(ro/size(m,1));

n = imtranslate(m, [dx, dy]);

%% TSaSSIE v2 Improvements

% New Templates
if ismember(folder, ["H868_OD" "H873_OS" "H878_OS" "H885_OD" "H897_OD"])
    n = 9;
elseif ismember(folder, ["H870_OD"])
    n = 8;
elseif ismember(folder, ["H678_OD" "H679_OS"])
    n = 5;
elseif ismember(folder, ["H680_OS" "H890_OD" "H901_OS" "H914_OD"])
    n = 4;
elseif ismember(folder, ["H871_OD" "H874_OS" "H885_OS" "H892_OS" "H895_OD" "H895_OS" "H900_OD"])
    n = 3;
elseif ismember(folder, ["H883_OS"])
    n = 2;
else
    n = 1;
end

% Translation was using circshift(circshift(image)) prior and more elseif
% calls than necessary - update using dx, dy instead of output(4),
% output(3) from dftregistration
fedge  = find(mean(moving_all),1);
[~,I] = (sort((abs(diff([0 mean(moving_all) 0]))),'descend'));
I = I-1;
moving_all = imtranslate(moving_all, [dx, dy]);
%moving_all = circshift(circshift(moving_all,round(output(4)),2),round(output(3)),1);
if 0 > fedge + round(dx)
    moving_all(:,size(moving_all,2)-abs(fedge + round(dx)):size(moving_all,2)) = 0;
elseif round(dx) ~= 0 || (max(I(1:2)) + abs(round(dx))) >= size(moving_all,2)
    moving_all(:,1:abs(size(im1_all,2)-abs(max(I(1:2)) + abs(round(dx))))) = 0;
end
if 0 < round(dy)
    moving_all(1:abs(round(dy)),:) = 0;
elseif round(dy) < 0
    moving_all(size(moving_all,1)-abs(round(dy)):end,:) = 0;
end

% New imregdemons paramtres work better for Sup, haven't tried other layers
[D, ~] = imregdemons(moving, (fixed_all),200,'AccumulatedFieldSmoothing',3.5, 'PyramidLevels', 4);