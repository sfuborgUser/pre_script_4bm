function volume_mcorr = localReg(volume, usfac, mcorrpath, fpsave, volNo, numBM)

numFr = size(volume, 3);
% yShift = zeros(1, size(volume, 3));
volume_mcorr = zeros(size(volume));


% We don't want to register BM scans anymore, we only need to apply the
% offset from the "leading" BM scan to the following BM scans. 
for jj = 1:numBM:(numFr-numBM)
    
    % [output, Greg] = dftregistration(ref, I, upsample) for reference
	[output, ~] = dftregistration(fft2(imgaussfilt(abs(volume(:, :, jj)), 2)), fft2(imgaussfilt(abs(volume(:, :, jj+numBM)), 2)), usfac);
	yShift(jj) = output(3);
	
	% For shifting the B scans, use nearest neighbour interpolation. The
	% sub pixel interpolation seems to cause horrendous line artifacts,
	% likely caused by bilinear interpolation on complex numbers
	for ii = (jj+numBM):(jj+2*numBM-1)
		volume_mcorr(:, :, ii) = imtranslate(volume(:, :, ii), [0, yShift(jj)], 'nearest');
	end
	
end

% save(fullfile(mcorrpath,[fpsave,'_',num2str(volNo-1),'    _mcorr.mat']), 'volume_mcorr', '-v7.3');

end


