% fn = '10_55_06-H772-OS-FOV_0    _mcorr';
% loadloc = 'D:\DATA\Patient\H772';
% load(fullfile(loadloc,fn));

% script_dir = 'D:\MJ\Dropbox\ProgramScripts\MatlabScripts\OCTViewer_Project';
% addpath(script_dir);

function cplxOCTA = proCplxOCTA(fn, savepath, loadloc)

%Load motion corrected volume and divide into the three frames
filename = strrep(fn,'.unp','    _mcorr.mat');
load(fullfile(loadloc,filename));
%Load Acquisition Parameters
parameters  = getParameters(strcat(fn(1:end-6),'.xml'));

%%%% Set parameters %%%%
numBMscans = parameters(5);
numFrames = size(volume_mcorr, 3);
strtFrame = 1;
cplxVol = volume_mcorr;   



% Average OCT %
avgCplxVol = zeros([size(cplxVol,1) size(cplxVol,2) ((size(cplxVol,3)/numBMscans))]);
for I = 1:numBMscans:size(cplxVol,3)
    K = ((I-1)/numBMscans)+1;
    for J = 1:numBMscans-1
        cplxConjX     = cplxVol(:,:,I+J).*conj(cplxVol(:,:,I));
        bulkOffset    = angle(sum(cplxConjX,1));
        avgCplxVol(:,:,K) = abs(avgCplxVol(:,:,K)) + abs(cplxVol(:,:,I+(J-1))...
            .*exp(-1j*repmat(bulkOffset, [size(cplxConjX,1) 1])));
    end
end
avgOctVol_dB = 20.*log10((avgCplxVol./(numBMscans)));
avgOctVol_dB = flipud(avgOctVol_dB);

% save(fullfile(savepath, [fn(1:end-4),'_avg_OctVol_dB.mat']),        'avgOctVol_dB', '-v7.3');


slowX1 = 1;
%% OCTA process %%
for I = 1:numBMscans:size(cplxVol,3)
    K = ((I-1)/numBMscans)+1;  
    if numBMscans == 2
        for J = 1:numBMscans-1
            Xconj    = cplxVol(:,:,I+J).*conj(cplxVol(:,:,I));
            BulkOff  = repmat(angle(sum(Xconj,1)), [size(Xconj,1) 1]);
            DiffCplx(:,:,J) = cplxVol(:,:,I+J).*exp(-1j*BulkOff) - cplxVol(:,:,I);
        end
        cplxOCTA(:,:,K) = abs(DiffCplx);
    else
        cplxBMscans = zeros(size(cplxVol,1), size(cplxVol,2), numBMscans);
        
        for idx_BM = 1:numBMscans
            Xconj    = cplxVol(:,:,I+(idx_BM-1)).*conj(cplxVol(:,:,I));
            BulkOff  = repmat(angle(sum(Xconj,1)), [size(Xconj,1) 1]);
            cplxBMscans(:,:,idx_BM) = cplxVol(:,:,I+(idx_BM-1)).*exp(-1j*BulkOff);
        end
        cplxOCTA(:,:,K) = var(abs(cplxBMscans), 0, 3);
%   		absOCTA(:,:,K) = var(abs(cplxBMscans), 0, 3);

    end
end

%%histogram equalization
% [hgram, ~] = imhist(mat2gray(cplxOCTA(:, :, 105)));
% 
% for i = 1:size(cplxOCTA, 3)
% 	cplxOCTA(:, :, i) = histeq(mat2gray(cplxOCTA(:, :, i)), hgram);
% end


save(fullfile(savepath, [fn(1:end-4),'_cplxOCTA.mat']),        'cplxOCTA', '-v7.3');

end



