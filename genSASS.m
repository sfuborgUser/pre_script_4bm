clear all;

path = '/ensc/IMAGEBORG/DATA/ECC/fundus/H859/2018.11.28/';

%set strings to print to file
commandString1 = '#PBS -W group_list=ensc-imageborg';
commandString2 = '#PBS -l nodes=1:ppn=1';
commandString3 = '#PBS -l walltime=23:00:00';
commandString4 = '#PBS -l pmem=26gb';
commandString5 = '#PBS -m bea';
commandString6 = 'cd /ensc/IMAGEBORG/STUDENTS/FRANCIS/GitRepo/Pre_script_4BM';

cd(path);

list = dir;

%delete the existing .sh file before opening another and writing because I
%suck at file I/O
if exist('/ensc/IMAGEBORG/DATA/ECC/scripts/final/OCT/runSASSIE.sh')
   delete('/ensc/IMAGEBORG/DATA/ECC/scripts/final/OCT/runSASSIE.sh'); 
end

for ii = 3:size(list, 1)
    
    cd(list(ii).name)
    
    folders = dir;
    
    for jj = 3:size(folders, 1)
        
        %write to file
        filename = folders(jj).name;
        fileNameFull = fullfile('/ensc/IMAGEBORG/DATA/ECC/scripts/final/OCT/',filename);
        fileID = fopen(strcat(fileNameFull, '-',list(ii).name),'w');
        fullpath = fullfile(pwd, filename);
        commandString = ['matlab -nodisplay -nodesktop -r "TSASSIE_GEN(''',fullpath,'''); exit"'];
        fprintf(fileID,'%s\n',commandString1);
        fprintf(fileID,'%s\n',commandString2);
        fprintf(fileID,'%s\n',commandString3);
        fprintf(fileID,'%s\n',commandString4);
        fprintf(fileID,'%s\n',commandString5);
        fprintf(fileID,'%s\n',commandString6);
        fprintf(fileID,'%s\n',commandString);
        fclose(fileID);  
        
        %create .sh file
        allJobsFileName = '/ensc/IMAGEBORG/DATA/ECC/scripts/final/OCT/runSASSIE.sh';
        fileID_ALL_JOBS = fopen(allJobsFileName, 'a');
        fprintf(fileID_ALL_JOBS,'%s\n',['qsub ', strcat(filename, '-', list(ii).name)]);
        fclose(fileID_ALL_JOBS);
        
    end
    
    cd('..');
end

clear all;

