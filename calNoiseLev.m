function medianNoise = calNoiseLev(input)

intensity = abs(input).^2;
%%% Determine noise ROI %%%

reshapeIntensity = reshape(intensity, [size(intensity, 1) (size(intensity, 2)*size(intensity, 3))]);
meanIntensity = mean(reshapeIntensity,2);
stdIntensity = std(reshapeIntensity,0,2);
multIntensity = meanIntensity.*stdIntensity;

[~, minIndx] = min(multIntensity);

boolenA = minIndx - 5;
boolenB = minIndx + 5;


if boolenA < 0
    strNoise = 1;
    endNoise = 11;
elseif boolenB >= size(intensity, 1)
    strNoise = size(intensity, 1) - 11;
    endNoise = size(intensity, 1) - 1;
else
    strNoise = boolenA;
    endNoise = boolenB;
end

noiseROI = input(strNoise:endNoise,:,1);

for i = 1:size(noiseROI, 2)
    realVarNoise(i) = var(real(noiseROI(:,i)));
    imagVarNoise(i) = var(imag(noiseROI(:,i)));
end

stdNoise = std(abs(realVarNoise).^2);
varNoise = realVarNoise+imagVarNoise;
medianNoise = median(varNoise);
noiseDB = 10*log10(median(varNoise));
noiseSTD = median(stdNoise);


