clear all;
addpath('/ensc/IMAGEBORG/STUDENTS/FRANCIS/GitRepo/Pre_script_4BM');

% cd('/ensc/IMAGEBORG/STUDENTS/FRANCIS/jmj_shared/(jmj_shared) Seymour_OCTA/eomtj/OD');
%cd('/ensc/IMAGEBORG/STUDENTS/FRANCIS/jmj_shared/(jmj_shared) Seymour_OCTA/eomtj/OS');
%cd('/ensc/IMAGEBORG/STUDENTS/FRANCIS/jmj_shared/(jmj_shared) Seymour_OCTA/ewpark/OD');
 cd('/ensc/IMAGEBORG/STUDENTS/FRANCIS/jmj_shared/(jmj_shared) Seymour_OCTA/ewpark/OS');


filenames = dir('*tif');


for kk = 1:size(filenames, 1)
   
    for rr = 1:298
        V4(:, :, rr) = imread(filenames(kk).name, rr);
    end
%     V5 = repmat(V4, [1, 1, 15]);
    V5 = double(V4);

    slope = 1; noop=@(M) M; negate = @(M) -M;
        surfDist1 = [200,50]; %was 150/50
        surflines = [4,1]; numSurf = size(surflines,1);
        instructions = {{[],[]},        [0],                   8, 4,            surfDist1,        10, noop;... %was noop
            {[1,1]},        [0],                   4, 2,            zeros(0,2),       10, noop;...
            {[2,1]},        [0],                   2, 1,            zeros(0,2),       10, noop;...
            {[3,1]},        [0],                   1, 1,            zeros(0,2),       2, noop;...
            };
    gc = graphCut3D_prior(V5, numSurf, slope, instructions, surflines,0);
    gcELM = squeeze(gc{1});
    % filter
    h = fspecial('disk', 30);
    gcONL = round(imfilter(gcELM,h,'replicate'));
    save(fullfile('segmentation', strcat(filenames(kk).name(1:end-4), '-gcONL.mat')), 'gcONL');

    
    
    slope = 1; noop=@(M) M; negate = @(M) -M;
    surfDist1 = [150,10]; %was 270/50
    surflines = [4,1];  numSurf = size(surflines,1);
    instructions = {{[],[]},            [0],                 8, 4,          surfDist1,       5, noop;...   % ILM
        {[1,2]},            [0],                 4, 2,          zeros(0,2),      5, noop;...
        {[2,1]},            [0],                 2, 1,          zeros(0,2),      5, noop;...
        {[3,1]},            [0],                 1, 1,          zeros(0,2),      5, noop;...  % IO based on ILM
        };
    gc = graphCut3D_prior(V5, numSurf, slope, instructions, surflines,0);
    gcILM = squeeze(gc{1});
    h = fspecial('disk',10);
    gcILM = round(imfilter(gcILM,h,'replicate'));
        save(fullfile('segmentation', strcat(filenames(kk).name(1:end-4), '-gcILM.mat')), 'gcILM');


    % mask
    [~, X, ~] = meshgrid(1:size(V5,2),1:size(V5,1),1:size(V5,3));
    tempvol = zeros([1 size(gcONL,1) size(gcONL,2)]); tempvol(1,:,:) = gcONL;
    tempvol= repmat(tempvol,[size(V5,1),1,1]);
    X = (X-3) > tempvol;
    BVvol2 = V5;
    BVvol2(find(X)) = 32768;
    % 2. Use ELM as reference and find three dark-to-bright interface
    slope = 1; noop=@(M) M; negate = @(M) -M;
    prior{1} = reshape(gcONL,[1 size(gcONL,1) size(gcONL,2)]);
    surfDist1 = [35,5; 35,5]; %was 35/10
    surflines = [4,1];  numSurf = size(surflines,1);
    instructions = {{[],[],[]},                        [0,  0, 0],                 8, 4,          surfDist1,                2, negate;...
        {[1,1],[1,2],[1,3]},               [0,  0, 0],                 4, 2,          surfDist1,      [5, 5, 5], negate;...
        {[2,2]},                                  [0],                 2, 1,          zeros(0,2),               2, negate;...
        {[3,1]},                                  [0],                 1, 1,          zeros(0,2),               2, negate;...
        };
    gc = graphCut3D_prior(BVvol2, numSurf, slope, instructions, surflines,prior);
    gcINL = squeeze(gc{1});

    % filter
    h = fspecial('disk', 25);
    gcINL = round(imfilter(gcINL,h,'replicate'));
    save(fullfile('segmentation', strcat(filenames(kk).name(1:end-4), '-gcINL.mat')), 'gcINL');
    
    
%     
%     slope = 1; noop=@(M) M; negate = @(M) -M;
%     surfDist1 = [35,30]; %was 270/50
%     surflines = [4,1];  numSurf = size(surflines,1);
%     instructions = {{[],[]},            [0],                 4, 2,          surfDist1,       10, negate;...   % ILM
%         {[1,2]},            [0],                 2, 1,          zeros(0,2),      10, negate;...
%         {[2,1]},            [0],                 2, 1,          zeros(0,2),      10, negate;...
%         {[3,1]},            [0],                 1, 1,          zeros(0,2),      5, negate;...  % IO based on ILM
%         };
%     gc = graphCut3D_prior(V5, numSurf, slope, instructions, surflines,0);
%     gcRLM = squeeze(gc{1});
%     h = fspecial('disk',10);
%     gcRLM = round(imfilter(gcRLM,h,'replicate'));
%     save(fullfile('segmentation', strcat(filenames(kk).name(1:end-4), '-gcRLM.mat')), 'gcRLM');

    
end

