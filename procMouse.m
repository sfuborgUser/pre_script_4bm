function procMouse(filename)

%cd to RAW folder
if contains(upper(filename(1:2)), '_')
    cd(fullfile('/ensc/IMAGEBORG/DATA/ECC/RAW', upper(filename(9:12))));
else
    cd(fullfile('/ensc/IMAGEBORG/DATA/ECC/RAW', upper(filename(10:13))));
end

Folder = pwd;
FileList = dir(fullfile(Folder, '**', '*.xml'));
    
%To find RAW directory
for ii = 1:size(FileList, 1)
    if strcmp(strcat(filename, '.xml'), FileList(ii).name)
        directory = FileList(ii).folder;
    end
end

cd(directory);

%To find unp filenames
unp_list = dir(fullfile(directory, '*.unp'));

fn2 = strcat(filenamexml,'.xml');


%Set file paths
rawpath = fullfile(directory);
qcpath = fullfile(directory(1:25), 'fundus', directory(29:end), num2str(parameters(8)));
mcorrpath = fullfile(directory(1:25), 'mcorr', directory(29:end));
octapath = fullfile(directory(1:25), 'sv', directory(29:end));
BVpath = fullfile(directory(1:25), 'BVsmooth', directory(29:end));
gcpath = fullfile(directory(1:25), 'Segment_gc', directory(29:end));

addpath(rawpath)

if contains(filename,'unp')
    fpsave = filename(1:end-4);
else
    fpsave = filename;
end


if ~exist(mcorrpath,'dir')
	mkdir(mcorrpath);
end

cd(rawpath);




end


