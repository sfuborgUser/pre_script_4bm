#include "mex.h"
#include "matrix.h"
#include "graph.h"
#include <stdio.h>
#include <limits>
using std::numeric_limits;

extern void _main();

const int numInputArgs  = 7;
const int numOutputArgs = 1;

// Function definitions.
// -----------------------------------------------------------------
void mexFunction (int nlhs, mxArray *plhs[],
		  int nrhs, const mxArray *prhs[]) {
  
  // Check to see if we have the correct number of input and output
  // arguments.
  if (nrhs != numInputArgs)
    mexErrMsgTxt("Incorrect number of input arguments");
  if (nlhs != numOutputArgs)
    mexErrMsgTxt("Incorrect number of output arguments");

  // Get the inputs.
  const mxArray * pnNodes = prhs[0];
  const mxArray * pfrom   = prhs[1];
  const mxArray * pto     = prhs[2];
  const mxArray * pweight = prhs[3];
  const mxArray * prweight = prhs[4];
  const mxArray * psourceWeight = prhs[5];
  const mxArray * psinkWeight = prhs[6];
  if (mxGetNumberOfElements(pnNodes) != 1)
        mexErrMsgTxt("nNodes must be a scalar");
  if (!mxIsInt32(pfrom) || !mxIsInt32(pto))
        mexErrMsgTxt("from and to must have class int32");
  if (!mxIsDouble(pweight) || !mxIsDouble(prweight) || !mxIsDouble(psourceWeight) || !mxIsDouble(psinkWeight))
        mexErrMsgTxt("weight, rweight, sourceWeight and sinkWeight must have class double");
    int nNodes = mxGetScalar(pnNodes);
    int * from   = (int*)mxGetPr(prhs[1]);
    int * to     = (int*)mxGetPr(prhs[2]);
    double * weight = mxGetPr(prhs[3]);
    double * rweight = mxGetPr(prhs[4]);
    double * sourceWeight = mxGetPr(prhs[5]);
    double * sinkWeight = mxGetPr(prhs[6]);

    if (mxGetNumberOfElements(pfrom) != mxGetNumberOfElements(pto) || 
        mxGetNumberOfElements(pfrom) != mxGetNumberOfElements(pweight)|| 
        mxGetNumberOfElements(pfrom) != mxGetNumberOfElements(prweight))
            mexErrMsgTxt("Parameters from, to and weight must have the same number of elements.");

    if (mxGetNumberOfElements(psinkWeight) != nNodes || mxGetNumberOfElements(psourceWeight)!=nNodes)
        mexErrMsgTxt("sinkWeight and sourceWeight must have nNodes elements.");

    for (int it=0;it<mxGetNumberOfElements(pfrom);it++)
    {
        if (from[it]>nNodes || to[it]>nNodes)
            mexErrMsgTxt("from and to should not have entries greater than nNodes.");
        if (from[it]<1 || to[it]<1)
            mexErrMsgTxt("from and to should not have entries less than 1.");
        if (weight[it]<0 || rweight[it]<0)
            mexErrMsgTxt("inter-node weights cannot be negative.");
    }

    int nArcs = mxGetNumberOfElements(pfrom);

	Graph<double,double,double> *g = new Graph<double,double,double>(nNodes, nArcs*2); 

    g -> add_node(nNodes); 

    for (int it=0;it<nArcs;it++)
  {
//       double w = mxIsFinite(weight[it])?weight[it]:1e10;
//       double r = mxIsFinite(rweight[it])?rweight[it]:1e10;
      
        g -> add_edge( from[it]-1, to[it]-1,    /* capacities */  weight[it], rweight[it]);
    }
    for (int it=0;it<nNodes;it++)
        g -> add_tweights( it,   /* capacities */  sourceWeight[it], sinkWeight[it] );

	int flow = g -> maxflow();

    plhs[0] = mxCreateLogicalMatrix(nNodes,1);

    bool* z = (bool*)mxGetPr(plhs[0]);

    for (int it=0;it<nNodes;it++)
    {
        if (g->what_segment(it) == Graph<double,double,double>::SOURCE)
            z[it]=true;
    }

	delete g;

}

