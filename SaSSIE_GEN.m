% im1 = imout(:,:,1);
% im2 = imout(:,:,ind);
% im2_opl = imout_opl(:,:,ind);
% im2_sup = imout_sup(:,:,ind);

% function [mcorr_all, mcorr_opl, mcorr_sup, mcorr_int, mcorr_in_all, mcorr_in_opl, mcorr_in_sup, mcorr_in_int] = SaSSIE_GEN(im1_all,im2_all,im2_opl,im2_sup, im2_int, ...
%     im2_in_all, im2_in_opl, im2_in_sup, im2_in_int)
function [mcorr_all, mcorr_opl, mcorr_sup, mcorr_int, mcorr_in_all, mcorr_in_opl, mcorr_in_sup, mcorr_in_int] = SaSSIE_GEN(im1_all,im2_all,im2_opl,im2_sup, im2_int)
% im1_or = im1;
im2_all_or = im2_all;
% im2_in_all_or = im2_in_all;

% Filter input images
h = fspecial('average', [2 2]);
im1_all = mat2gray(double(im1_all));
im1_all = imfilter(im1_all,h);
im2_all = mat2gray(double(im2_all));
im2_all = imfilter(im2_all,h);
% im2_in_all = mat2gray(double(im2_in_all));
% im2_in_all = imfilter(im2_in_all,h);

% Specify range of motion & window size
romA = 7;
romL = 7;
winSize = 15;

im12_all = padarray(im1_all,[ceil(winSize/2)+romA ceil(winSize/2)+romA]);
im22_all = padarray(im2_all,[ceil(winSize/2)+romA ceil(winSize/2)+romA]);
im22_all_or = padarray(im2_all_or,[ceil(winSize/2)+romA ceil(winSize/2)+romA]);
im22_opl = padarray(im2_opl,[ceil(winSize/2)+romA ceil(winSize/2)+romA]);
im22_sup = padarray(im2_sup,[ceil(winSize/2)+romA ceil(winSize/2)+romA]);
im22_int = padarray(im2_int,[ceil(winSize/2)+romA ceil(winSize/2)+romA]);

% im22_in_all = padarray(im2_in_all,[ceil(winSize/2)+romA ceil(winSize/2)+romA]);
% im22_in_all_or = padarray(im2_in_all_or,[ceil(winSize/2)+romA ceil(winSize/2)+romA]);
% im22_in_opl = padarray(im2_in_opl,[ceil(winSize/2)+romA ceil(winSize/2)+romA]);
% im22_in_sup = padarray(im2_in_sup,[ceil(winSize/2)+romA ceil(winSize/2)+romA]);
% im22_in_int = padarray(im2_in_int,[ceil(winSize/2)+romA ceil(winSize/2)+romA]);

% Initialize variables
motionA = zeros(1,size(im1_all,1));
motionL = zeros(1,size(im1_all,2));
mcorr_all = zeros(size(im1_all));
mcorr_opl = zeros(size(im1_all));
mcorr_sup = zeros(size(im1_all));
mcorr_int = zeros(size(im1_all));
% mcorr_in_all = zeros(size(im1_all));
% mcorr_in_opl = zeros(size(im1_all));
% mcorr_in_sup = zeros(size(im1_all));
% mcorr_in_int = zeros(size(im1_all));
yy = zeros(size(im1_all));
xx = zeros(size(im1_all));

% Find maximum cross-corelation
for i = (1+ceil(winSize/2)+romA:size(im12_all,1)-ceil(winSize/2)-romA)
    
    for j = (1+ceil(winSize/2)+romA:size(im12_all,2)-ceil(winSize/2)-romA)
        mask = im12_all(i-floor(winSize/2):i+floor(winSize/2), j-floor(winSize/2):j+floor(winSize/2));
        field_all = im22_all(i-floor(winSize/2)-romA:i+floor(winSize/2)+romA, j-floor(winSize/2)-romL:j+floor(winSize/2)+romL);
        field_all_or = im22_all_or(i-floor(winSize/2)-romA:i+floor(winSize/2)+romA, j-floor(winSize/2)-romL:j+floor(winSize/2)+romL);
        field_opl = im22_opl(i-floor(winSize/2)-romA:i+floor(winSize/2)+romA, j-floor(winSize/2)-romL:j+floor(winSize/2)+romL);
        field_sup = im22_sup(i-floor(winSize/2)-romA:i+floor(winSize/2)+romA, j-floor(winSize/2)-romL:j+floor(winSize/2)+romL);
        field_int = im22_int(i-floor(winSize/2)-romA:i+floor(winSize/2)+romA, j-floor(winSize/2)-romL:j+floor(winSize/2)+romL);
        
%         field_in_all_or = im22_in_all_or(i-floor(winSize/2)-romA:i+floor(winSize/2)+romA, j-floor(winSize/2)-romL:j+floor(winSize/2)+romL);
%         field_in_opl = im22_in_opl(i-floor(winSize/2)-romA:i+floor(winSize/2)+romA, j-floor(winSize/2)-romL:j+floor(winSize/2)+romL);
%         field_in_sup = im22_in_sup(i-floor(winSize/2)-romA:i+floor(winSize/2)+romA, j-floor(winSize/2)-romL:j+floor(winSize/2)+romL);
%         field_in_int = im22_in_int(i-floor(winSize/2)-romA:i+floor(winSize/2)+romA, j-floor(winSize/2)-romL:j+floor(winSize/2)+romL);
        
        if  im22_all(i,j) ==0 %median(median(field)) ==0 %range(mask(:)) < 0.15 || (max(max(field))<0.001) ||
            motionA(i) = 0;
            motionL(j) = 0;
            mcorr_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = im22_all_or(i,j);
            mcorr_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = im22_opl(i,j);
            mcorr_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = im22_sup(i,j);
            mcorr_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = im22_int(i,j);
%             mcorr_in_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = im22_in_all_or(i,j);
%             mcorr_in_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = im22_in_opl(i,j);
%             mcorr_in_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = im22_in_sup(i,j);
%             mcorr_in_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = im22_in_int(i,j);
            
            continue
        else
            
            c = normxcorr2(mask,field_all);
            c(:,:,2) = normxcorr2(mask,imrotate(field_all,-15,'bilinear','crop'));
            c(:,:,3) = normxcorr2(mask,imrotate(field_all,-10,'bilinear','crop'));
            c(:,:,4) = normxcorr2(mask,imrotate(field_all,-5,'bilinear','crop'));
            c(:,:,5) = normxcorr2(mask,imrotate(field_all,5,'bilinear','crop'));
            c(:,:,6) = normxcorr2(mask,imrotate(field_all,10,'bilinear','crop'));
            c(:,:,7) = normxcorr2(mask,imrotate(field_all,15,'bilinear','crop'));
            
            c = c(size(mask,1)+floor(size(mask,1)/2):end-floor(size(mask,1)/2),size(mask,2)+floor(size(mask,2)/2):end-floor(size(mask,1)/2),:);
            cc = max(c,[],3);
            
            [ypeak, xpeak, zpeak] = ind2sub(size(c),find(c==max(c(:))));
            cc_offset = [ypeak+size(mask,1)-1, xpeak+size(mask,1)-1];
            
            if zpeak ==1
                mcorr_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = field_all_or(cc_offset(1),cc_offset(2));
                mcorr_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = field_opl(cc_offset(1),cc_offset(2));
                mcorr_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = field_sup(cc_offset(1),cc_offset(2));
                mcorr_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = field_int(cc_offset(1),cc_offset(2));
%                 mcorr_in_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = field_in_all_or(cc_offset(1),cc_offset(2));
%                 mcorr_in_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = field_in_opl(cc_offset(1),cc_offset(2));
%                 mcorr_in_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = field_in_sup(cc_offset(1),cc_offset(2));
%                 mcorr_in_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = field_in_int(cc_offset(1),cc_offset(2));
            elseif zpeak == 2
                f_all = imrotate(field_all_or,-15,'bilinear','crop');
                mcorr_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_all(cc_offset(1),cc_offset(2));
                f_opl = imrotate(field_opl,-15,'bilinear','crop');
                mcorr_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_opl(cc_offset(1),cc_offset(2));
                f_sup = imrotate(field_sup,-15,'bilinear','crop');
                mcorr_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_sup(cc_offset(1),cc_offset(2));
                f_int = imrotate(field_int,-15,'bilinear','crop');
                mcorr_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_int(cc_offset(1),cc_offset(2));
%                 f_in_all = imrotate(field_in_all_or,-15,'bilinear','crop');
%                 mcorr_in_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_all(cc_offset(1),cc_offset(2));
%                 f_in_opl = imrotate(field_in_opl,-15,'bilinear','crop');
%                 mcorr_in_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_opl(cc_offset(1),cc_offset(2));
%                 f_in_sup = imrotate(field_in_sup,-15,'bilinear','crop');
%                 mcorr_in_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_sup(cc_offset(1),cc_offset(2));
%                 f_in_int = imrotate(field_in_int,-15,'bilinear','crop');
%                 mcorr_in_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_int(cc_offset(1),cc_offset(2));
            elseif zpeak == 3
                f_all = imrotate(field_all_or,-10,'bilinear','crop');
                mcorr_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_all(cc_offset(1),cc_offset(2));
                f_opl = imrotate(field_opl,-10,'bilinear','crop');
                mcorr_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_opl(cc_offset(1),cc_offset(2));
                f_sup = imrotate(field_sup,-10,'bilinear','crop');
                mcorr_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_sup(cc_offset(1),cc_offset(2));
                f_int = imrotate(field_int,-10,'bilinear','crop');
                mcorr_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_int(cc_offset(1),cc_offset(2));
%                 f_in_all = imrotate(field_in_all_or,-10,'bilinear','crop');
%                 mcorr_in_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_all(cc_offset(1),cc_offset(2));
%                 f_in_opl = imrotate(field_in_opl,-10,'bilinear','crop');
%                 mcorr_in_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_opl(cc_offset(1),cc_offset(2));
%                 f_in_sup = imrotate(field_in_sup,-10,'bilinear','crop');
%                 mcorr_in_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_sup(cc_offset(1),cc_offset(2));
%                 f_in_int = imrotate(field_in_int,-10,'bilinear','crop');
%                 mcorr_in_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_int(cc_offset(1),cc_offset(2));
            elseif zpeak == 4
                f_all = imrotate(field_all_or,-5,'bilinear','crop');
                mcorr_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_all(cc_offset(1),cc_offset(2));
                f_opl = imrotate(field_opl,-5,'bilinear','crop');
                mcorr_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_opl(cc_offset(1),cc_offset(2));
                f_sup = imrotate(field_sup,-5,'bilinear','crop');
                mcorr_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_sup(cc_offset(1),cc_offset(2));
                f_int = imrotate(field_int,-5,'bilinear','crop');
                mcorr_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_int(cc_offset(1),cc_offset(2));
%                 f_in_all = imrotate(field_in_all_or,-5,'bilinear','crop');
%                 mcorr_in_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_all(cc_offset(1),cc_offset(2));
%                 f_in_opl = imrotate(field_in_opl,-5,'bilinear','crop');
%                 mcorr_in_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_opl(cc_offset(1),cc_offset(2));
%                 f_in_sup = imrotate(field_in_sup,-5,'bilinear','crop');
%                 mcorr_in_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_sup(cc_offset(1),cc_offset(2));
%                 f_in_int = imrotate(field_in_int,-5,'bilinear','crop');
%                 mcorr_in_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_int(cc_offset(1),cc_offset(2));

            elseif zpeak == 5
                f_all = imrotate(field_all_or,5,'bilinear','crop');
                mcorr_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_all(cc_offset(1),cc_offset(2));
                f_opl = imrotate(field_opl,5,'bilinear','crop');
                mcorr_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_opl(cc_offset(1),cc_offset(2));
                f_sup = imrotate(field_sup,5,'bilinear','crop');
                mcorr_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_sup(cc_offset(1),cc_offset(2));
                f_int = imrotate(field_int,5,'bilinear','crop');
                mcorr_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_int(cc_offset(1),cc_offset(2));
%                 f_in_all = imrotate(field_in_all_or,5,'bilinear','crop');
%                 mcorr_in_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_all(cc_offset(1),cc_offset(2));
%                 f_in_opl = imrotate(field_in_opl,5,'bilinear','crop');
%                 mcorr_in_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_opl(cc_offset(1),cc_offset(2));
%                 f_in_sup = imrotate(field_in_sup,5,'bilinear','crop');
%                 mcorr_in_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_sup(cc_offset(1),cc_offset(2));
%                 f_in_int = imrotate(field_in_int,5,'bilinear','crop');
%                 mcorr_in_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_int(cc_offset(1),cc_offset(2));
            elseif zpeak == 6
                f_all = imrotate(field_all_or,10,'bilinear','crop');
                mcorr_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_all(cc_offset(1),cc_offset(2));
                f_opl = imrotate(field_opl,10,'bilinear','crop');
                mcorr_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_opl(cc_offset(1),cc_offset(2));
                f_sup = imrotate(field_sup,10,'bilinear','crop');
                mcorr_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_sup(cc_offset(1),cc_offset(2));
                f_int = imrotate(field_int,10,'bilinear','crop');
                mcorr_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_int(cc_offset(1),cc_offset(2));
%                 f_in_all = imrotate(field_in_all_or,10,'bilinear','crop');
%                 mcorr_in_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_all(cc_offset(1),cc_offset(2));
%                 f_in_opl = imrotate(field_in_opl,10,'bilinear','crop');
%                 mcorr_in_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_opl(cc_offset(1),cc_offset(2));
%                 f_in_sup = imrotate(field_in_sup,10,'bilinear','crop');
%                 mcorr_in_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_sup(cc_offset(1),cc_offset(2));
%                 f_in_int = imrotate(field_in_int,10,'bilinear','crop');
%                 mcorr_in_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_int(cc_offset(1),cc_offset(2));
            else
                f_all = imrotate(field_all_or,15,'bilinear','crop');
                mcorr_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_all(cc_offset(1),cc_offset(2));
                f_opl = imrotate(field_opl,15,'bilinear','crop');
                mcorr_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_opl(cc_offset(1),cc_offset(2));
                f_sup = imrotate(field_sup,15,'bilinear','crop');
                mcorr_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_sup(cc_offset(1),cc_offset(2));
                f_int = imrotate(field_int,15,'bilinear','crop');
                mcorr_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_int(cc_offset(1),cc_offset(2));
%                 f_in_all = imrotate(field_in_all_or,15,'bilinear','crop');
%                 mcorr_in_all(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_all(cc_offset(1),cc_offset(2));
%                 f_in_opl = imrotate(field_in_opl,15,'bilinear','crop');
%                 mcorr_in_opl(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_opl(cc_offset(1),cc_offset(2));
%                 f_in_sup = imrotate(field_in_sup,15,'bilinear','crop');
%                 mcorr_in_sup(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_sup(cc_offset(1),cc_offset(2));
%                 f_in_int = imrotate(field_in_int,15,'bilinear','crop');
%                 mcorr_in_int(i-(ceil(winSize/2)+romA),j-(ceil(winSize/2)+romA)) = f_in_int(cc_offset(1),cc_offset(2));
            end
            
            yy(i,j) = cc_offset(1);
            xx(i,j) = cc_offset(2);
        end
    end
end

end
